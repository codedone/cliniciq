FROM openjdk:8-jdk-alpine
ARG JAR_FILE
COPY ./target/${JAR_FILE} /opt
ENTRYPOINT ["java","-jar", "/opt/cliniciq_evotor_integration.jar"]
EXPOSE 9099