package cliniciq.evotor.service;

import cliniciq.evotor.db.domain.cashbox.*;
import cliniciq.evotor.db.domain.rest.EvotorCheck;
import cliniciq.evotor.db.service.cashbox.EvotorPosUserService;
import cliniciq.evotor.db.service.cashbox.EvotorServiceService;
import cliniciq.evotor.db.service.cashbox.EvotorShopService;
import cliniciq.evotor.db.service.cashbox.EvotorUserService;
import cliniciq.evotor.mapper.model.Position;
import cliniciq.evotor.mapper.model.Receipt;
import cliniciq.evotor.utils.exceptions.mapping.CannotFindEvotorPosUserMappingException;
import cliniciq.evotor.utils.exceptions.mapping.CannotFindPaymentUUIDMappingException;
import cliniciq.evotor.utils.exceptions.mapping.CheckMappingException;
import com.dsystem.utils.DSystemConstants;
import com.dsystem.utils.data.wrapper.PageWrapper;
import com.dsystem.utils.data.wrapper.PageWrapperImpl;
import com.dsystem.utils.rest.wrapper.ResultDataWrapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static cliniciq.evotor.db.domain.enums.CheckStatus.PROCESSED;

@Service
public class CheckMappingService {

    private final EvotorShopService evotorShopService;
    private final EvotorUserService evotorUserService;
    private final EvotorServiceService evotorServiceService;
    private final EvotorPosUserService evotorPosUserService;

    @Autowired
    public CheckMappingService(EvotorShopService evotorShopService,
                               EvotorUserService evotorUserService,
                               EvotorServiceService evotorServiceService,
                               EvotorPosUserService evotorPosUserService) {
        this.evotorShopService = evotorShopService;
        this.evotorUserService = evotorUserService;
        this.evotorServiceService = evotorServiceService;
        this.evotorPosUserService = evotorPosUserService;
    }

    public EvotorCheck map(ClinicIQCheck clinicIQCheck) throws IOException {
        String cliniciqJson = clinicIQCheck.getCheckJson();
        Receipt receipt = mapReceipt(cliniciqJson);

        for (Position position : receipt.getPositions()) {
            EvotorService evotorService = evotorServiceService.findByDSystemALTCode(position.getProductUuid());
                if (Objects.isNull(evotorService)) {
                    throw new CannotFindPaymentUUIDMappingException("Cannot find evotor payment UUID with dSystemALTCode : " + position.getProductUuid());
                } else {
                    position.setProductCode(evotorService.getEvotorServiceUUID());
                }
            }

        EvotorShop evotorShop = evotorShopService.findByDSystemDomainId(receipt.getExtra().getDomainId());
        EvotorUser evotorUser = evotorUserService.findByDSystemUserId(receipt.getExtra().getUserId());
        receipt.setExtra(null);

        String deviceUUID = findDeviceUUID(evotorShop, evotorUser);
        receipt.setEvotorDeviceId(deviceUUID);

        String evotorJson = mapJson(toResultWrapper(receipt));

        return new EvotorCheck()
                .setShopUUID((Objects.nonNull(evotorShop)) ? evotorShop.getEvotorUUID() : null)
                .setUserUUID((Objects.nonNull(evotorUser)) ? evotorUser.getEvotorUserUUID() : null)
                .setCheckStatus(PROCESSED)
                .setDeviceUUID(deviceUUID)
                .setCheckJson(evotorJson);
    }

    private String findDeviceUUID(EvotorShop evotorShop, EvotorUser evotorUser) {

        if (Objects.nonNull(evotorShop) && Objects.nonNull(evotorUser)) {
            EvotorPosUser evotorPosUser = evotorPosUserService.findByEvotorUserAndEvotorShop(evotorShop, evotorUser);
            if (Objects.nonNull(evotorPosUser)) {
                return evotorPosUser.getEvotorDeviceId();
            }
        }

        if (Objects.nonNull(evotorShop)) {
            List<EvotorPosUser> evotorPosUsers = evotorPosUserService.findByEvotorShop(evotorShop);
            if (Objects.nonNull(evotorPosUsers) && evotorPosUsers.size() == 1) {
                return evotorPosUsers.get(0).getEvotorDeviceId();
            }
        }

        Integer shopId = (Objects.isNull(evotorShop)) ? null : evotorShop.getDSystemDomainId();
        Integer userId = (Objects.isNull(evotorUser)) ? null : evotorUser.getDSystemUserId();
        throw new CannotFindEvotorPosUserMappingException("Cannot find evotor pos user UUID with shop id:" + shopId + ", user id:" + userId);
    }

    private ResultDataWrapper toResultWrapper(Receipt receipt) {
        ResultDataWrapper resultDataWrapper = new ResultDataWrapper();
        resultDataWrapper.setResultCode(DSystemConstants.SUCCESS);
        PageWrapper pageWrapper = new PageWrapperImpl.Builder().build().setData(receipt);
        resultDataWrapper.setPageWrapper(pageWrapper);
        return resultDataWrapper;
    }

    private Receipt mapReceipt(String json) throws IOException {
        JSONObject jsonObject = new JSONObject(json);
        JSONObject dataJsonObject = jsonObject
                .getJSONObject("pageWrapper")
                .getJSONObject("data");

        ObjectMapper mapper = new ObjectMapper();
        Receipt receipt = mapper.readValue(dataJsonObject.toString(), Receipt.class);
        if (Objects.isNull(receipt.getExtra())) {
            throw new CheckMappingException("Cannot map object without object Extra (domainId, userId)");
        }
        return receipt;
    }

    private String mapJson(ResultDataWrapper resultDataWrapper) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(resultDataWrapper);
    }
}
