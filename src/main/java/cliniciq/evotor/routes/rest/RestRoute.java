package cliniciq.evotor.routes.rest;

import cliniciq.evotor.config.evotor.CheckSearchRouteConfiguration;
import cliniciq.evotor.config.evotor.NotificationRouteConfiguration;
import cliniciq.evotor.config.evotor.SideServiceUserRouteConfiguration;
import cliniciq.evotor.config.evotor.TokenProcessorRouteConfiguration;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.MediaType;

@Component
public class RestRoute extends RouteBuilder {

    @Value("${service.port:9009}")
    String serverPort;

    private final NotificationRouteConfiguration notificationRouteConfiguration;
    private final CheckSearchRouteConfiguration checkSearchRouteConfiguration;
    private final SideServiceUserRouteConfiguration sideServiceUserRouteConfiguration;
    private final TokenProcessorRouteConfiguration tokenProcessorRouteConfiguration;

    public RestRoute(NotificationRouteConfiguration notificationRouteConfiguration,
                     CheckSearchRouteConfiguration checkSearchRouteConfiguration,
                     SideServiceUserRouteConfiguration sideServiceUserRouteConfiguration,
                     TokenProcessorRouteConfiguration tokenProcessorRouteConfiguration) {
        this.notificationRouteConfiguration = notificationRouteConfiguration;
        this.checkSearchRouteConfiguration = checkSearchRouteConfiguration;
        this.sideServiceUserRouteConfiguration = sideServiceUserRouteConfiguration;
        this.tokenProcessorRouteConfiguration = tokenProcessorRouteConfiguration;
    }

    @Override
    public void configure() {
        // @formatter:off

        restConfiguration()
                .dataFormatProperty("prettyPrint", "true")
                .port(serverPort)
                .enableCORS(true)
                .contextPath("/api")
                .component("spark-rest");

        rest("/v1")
                // -----------------------------------------------------------//
                .post("/user/token")
                    .description("Service for Evotor Token consumption")
                    .id("evotor_token")
                    .consumes(MediaType.APPLICATION_JSON)
                    .produces(MediaType.APPLICATION_JSON)
                .to(tokenProcessorRouteConfiguration.authPath())

                // -----------------------------------------------------------//
                .post("/user/verify")
                    .description("Evotor Side Service User Authentication")
                    .id("evotor_side_service_user_auth")
                    .consumes(MediaType.APPLICATION_JSON)
                    .produces(MediaType.APPLICATION_JSON)
                .to(sideServiceUserRouteConfiguration.authPath())

                // -----------------------------------------------------------//
                .post("/user/created")
                    .description("Evotor Side Service User Creation")
                    .id("evotor_side_service_user_creation")
                    .consumes(MediaType.APPLICATION_JSON)
                    .produces(MediaType.APPLICATION_JSON)
                .to(sideServiceUserRouteConfiguration.creationPath())

                // -----------------------------------------------------------//
                .put("/inventories/devices")
                    .description("Service Evotor Device created")
                    .id("evotor_device_created")
                    .consumes(MediaType.APPLICATION_JSON)
                    .produces(MediaType.APPLICATION_JSON)
                .to(notificationRouteConfiguration.posPath())

                // -----------------------------------------------------------//
                .put("/inventories/employees")
                    .description("Service Evotor User created")
                    .id("evotor_user_created")
                    .consumes(MediaType.APPLICATION_JSON)
                    .produces(MediaType.APPLICATION_JSON)
                .to(notificationRouteConfiguration.userPath())

                // -----------------------------------------------------------//
                .put("/inventories/stores")
                    .description("Service Evotor Store created")
                    .id("evotor_store_created")
                    .consumes(MediaType.APPLICATION_JSON)
                    .produces(MediaType.APPLICATION_JSON)
                .to(notificationRouteConfiguration.shopPath())

                // -----------------------------------------------------------//
                .put("/inventories/{storeUUID}/products")
                    .description("Service Evotor Products created")
                    .id("evotor_products_created")
                    .consumes(MediaType.APPLICATION_JSON)
                    .produces(MediaType.APPLICATION_JSON)
                .to(notificationRouteConfiguration.servicesPath());

        // -----------------------------------------------------------//
        rest("/v2")
                .put("/receipts")
                .description("Service Evotor Receipt printed")
                .id("evotor_receipt_printed")
                .consumes(MediaType.APPLICATION_JSON)
                .produces(MediaType.APPLICATION_JSON)
                .to(notificationRouteConfiguration.checkPath());

        // -----------------------------------------------------------//
        rest("/payment")
                .post("/check")
                .description("Service Evotor check")
                .id("service_evotor_check")
                .consumes(MediaType.APPLICATION_JSON)
                .produces(MediaType.APPLICATION_JSON)
                .to(checkSearchRouteConfiguration.checkSearchPath());

        // TODO used in rotes where logic is not yet implemented
        from("direct:logMessage")
                .log("Received from Evotor >>> ${body}")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200));

        // @formatter:on
    }
}
