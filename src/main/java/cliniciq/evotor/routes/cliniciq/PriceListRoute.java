package cliniciq.evotor.routes.cliniciq;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class PriceListRoute extends RouteBuilder {

    //INITIAL price update

/*
    private static final String AUTH_PROCESSOR_ROUTE = "direct:price_list_update_auth";
    private static final String PRICE_LIST_LOAD_PROCESSOR_ROUTE = "direct:price_list_update_load";

    private static final String ORIGINAL_CLINICIQ_PRICE_LIST = "original_cliniciq_price_list";

    @Value(value = "${cron.priceListUpdateCron:30+5+*+*+*}")
    private String PRICE_LIST_UPDATE_CRON;

    @Value("${cliniciq.base.url}")
    private String cliniciqAuthUrl;

    @Value("${cliniciq.username}")
    private String username;

    @Value("${cliniciq.password}")
    private String password;
*/

    @Override
    public void configure() {
        // @formatter:off

       /* //TODO restore evotor price list exception occurred

        from("quartz2://price_list_update?cron=" + PRICE_LIST_UPDATE_CRON)
                .routeId("mapping_retry_scheduler")
                .log(LoggingLevel.DEBUG, log, "Retry Mapping Check errors")
                .to(AUTH_PROCESSOR_ROUTE)
                .to(PRICE_LIST_LOAD_PROCESSOR_ROUTE)
                //TODO .to(save price list from ClinicIQ)
                //TODO .to(load price list from evotor)
                //TODO .to(store price list from evotor)
                //TODO .to(remove price list from evotor)
                //TODO .to(send new price list to evotor)
                .log(LoggingLevel.DEBUG, log, "Done trying Mapping Check errors");

        from(AUTH_PROCESSOR_ROUTE)
                .description("ClinicIQ user Authentication service Call")
                .routeId("cliniciq_user_authentication_service_call")
                .setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.POST)
                .setHeader("Username").constant(username)
                .setHeader("Password").constant(password)
                .setBody(constant("client_credentials"))
                .setHeader(Exchange.HTTP_QUERY, constant("grant_type=client_credentials"))
                .toD(cliniciqAuthUrl + "/facade/oauth/token?bridgeEndpoint=true" +
                        "&authMethod=Basic" +
                        "&authUsername=${header.username}" +
                        "&authPassword=${header.password}" +
                        "&httpClient.authenticationPreemptive=true")
                .bean(PriceListRoute.class, "extractLoginInfo")
                .setBody(constant(""))
                .end();

        from(PRICE_LIST_LOAD_PROCESSOR_ROUTE)
                .description("ClinicIQ price list load call")
                .routeId("cliniciq_price_list_load_call")
                .setHeader(Exchange.HTTP_METHOD).constant(HttpMethod.GET)
                .setHeader("Authorization").simple("${header.authorization}")
                .setHeader("sessionId").simple("${header.sessionId}")
                .setBody(constant(""))//TODO clarify how exactly date is formatted "В body добавляем JSON с указанием дня для которого нужно вернуть расписание"
                .toD(cliniciqAuthUrl + "/pricelist/items?bridgeEndpoint=true")
                .convertBodyTo(String.class)
                .setProperty(ORIGINAL_CLINICIQ_PRICE_LIST, simple("${body}"))
                .end();*/

        // @formatter:on
    }

/*    private void extractLoginInfo(@JsonPath("$.access_token") String accessToken,
                                  @JsonPath("$.sessionId") String sessionId,
                                  Exchange exchange){
        exchange.setProperty("authorization", accessToken);
        exchange.setProperty("sessionId", sessionId);
    }*/
}