package cliniciq.evotor.routes.cliniciq;

import cliniciq.evotor.config.KafkaRouteConfiguration;
import cliniciq.evotor.db.domain.cashbox.ClinicIQCheck;
import cliniciq.evotor.db.domain.enums.CheckStatus;
import cliniciq.evotor.db.service.cashbox.ClinicIQCheckService;
import cliniciq.evotor.db.service.rest.EvotorCheckService;
import cliniciq.evotor.service.CheckMappingService;
import cliniciq.evotor.utils.camel.KafkaCamelUtils;
import cliniciq.evotor.utils.exceptions.db.CheckRequestSaveDBException;
import cliniciq.evotor.utils.exceptions.db.CheckResponseSaveDBException;
import cliniciq.evotor.utils.exceptions.mapping.CheckMappingException;
import org.apache.camel.Body;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import static cliniciq.evotor.utils.enums.KafkaRouteType.CONSUMER;

@Profile("!no_kafka")
@Component
public class CheckConsumerRoute extends RouteBuilder {

    private final KafkaRouteConfiguration kafkaRouteConfiguration;
    private final CheckMappingService checkMappingService;
    private final ClinicIQCheckService clinicIQCheckService;
    private final EvotorCheckService evotorCheckService;

    @Autowired
    public CheckConsumerRoute(KafkaRouteConfiguration kafkaRouteConfiguration,
                              CheckMappingService checkMappingService,
                              ClinicIQCheckService clinicIQCheckService,
                              EvotorCheckService evotorCheckService) {
        this.kafkaRouteConfiguration = kafkaRouteConfiguration;
        this.checkMappingService = checkMappingService;
        this.clinicIQCheckService = clinicIQCheckService;
        this.evotorCheckService = evotorCheckService;
    }

    @Override
    public void configure() {
        // @formatter:off

        onException(CheckRequestSaveDBException.class)
                .handled(true)
                .log(LoggingLevel.ERROR, log, "CheckRequestSaveDBException occurred in CheckConsumerRoute, dont commit offset to kafka")
                .log(LoggingLevel.ERROR, log, "${exception.message}")
                .setBody(constant(""))
                .stop();

        onException(CheckResponseSaveDBException.class)
                .handled(true)
                .log(LoggingLevel.ERROR, log, "CheckResponseSaveDBException occurred in CheckConsumerRoute")
                .log(LoggingLevel.ERROR, log, "${exception.message}")
                .bean(clinicIQCheckService, "markAsError")
                .setBody(constant(""))
                .stop();

        onException(CheckMappingException.class)
                .handled(true)
                .log(LoggingLevel.ERROR, log, "CheckMappingException occurred in CheckConsumerRoute")
                .log(LoggingLevel.ERROR, log, "${exception.message}")
                .bean(clinicIQCheckService, "markAsError")
                .setBody(constant(""))
                .stop();

        // -----------------------------------------------------------//
        from(kafkaRouteConfiguration.checkTopic(CONSUMER))
                .routeId("cliniciq_check")
                .to("seda:cliniciq_check_consumer_processor")
                .process(KafkaCamelUtils::manualCommit);

        from("seda:cliniciq_check_consumer_processor")
                .log("${body}")
                .bean(CheckConsumerRoute.class, "transformToClinicIQCheck")
                .bean(clinicIQCheckService, "save")
                .bean(checkMappingService, "map")
                .bean(evotorCheckService, "save")
                .end()
                .setId("cliniciq_check_consumer_processor");

        // @formatter:on
    }

    //TODO clarify header parameters
    public ClinicIQCheck transformToClinicIQCheck(@Body String body) {
        return new ClinicIQCheck()
                .setCheckStatus(CheckStatus.RECEIVED)
                .setCheckJson(body);
    }
}