package cliniciq.evotor.routes.cliniciq;

import cliniciq.evotor.db.service.cashbox.ClinicIQCheckService;
import cliniciq.evotor.db.service.rest.EvotorCheckService;
import cliniciq.evotor.service.CheckMappingService;
import cliniciq.evotor.utils.exceptions.db.CheckRequestSaveDBException;
import cliniciq.evotor.utils.exceptions.db.CheckResponseSaveDBException;
import cliniciq.evotor.utils.exceptions.mapping.CheckMappingException;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CheckErrorRetryRoute extends RouteBuilder {

    private final CheckMappingService checkMappingService;
    private final ClinicIQCheckService clinicIQCheckService;
    private final EvotorCheckService evotorCheckService;

    @Value(value = "${cron.remapping:00+*/5+*+1/1+*+?+*}")
    private String RETRY_CRON;

    @Autowired
    public CheckErrorRetryRoute(CheckMappingService checkMappingService,
                                ClinicIQCheckService clinicIQCheckService,
                                EvotorCheckService evotorCheckService) {
        this.checkMappingService = checkMappingService;
        this.clinicIQCheckService = clinicIQCheckService;
        this.evotorCheckService = evotorCheckService;
    }

    @Override
    public void configure() {
        // @formatter:off

        onException(CheckRequestSaveDBException.class)
                .handled(true)
                .log(LoggingLevel.ERROR, log, "CheckRequestSaveDBException occurred in CheckErrorRetryRoute, dont commit offset to kafka")
                .log(LoggingLevel.ERROR, log, "${exception.message}")
                .setBody(constant(""))
                .stop();

        onException(CheckResponseSaveDBException.class)
                .handled(true)
                .log(LoggingLevel.ERROR, log, "CheckResponseSaveDBException occurred in CheckErrorRetryRoute")
                .log(LoggingLevel.ERROR, log, "${exception.message}")
                .bean(clinicIQCheckService, "markAsError")
                .setBody(constant(""))
                .stop();

        onException(CheckMappingException.class)
                .handled(true)
                .log(LoggingLevel.ERROR, log, "CheckMappingException occurred in CheckConsumerRoute")
                .log(LoggingLevel.ERROR, log, "${exception.message}")
                .bean(clinicIQCheckService, "markAsError")
                .setBody(constant(""))
                .stop();

        // -----------------------------------------------------------//
        from("quartz2://mapping_retry?cron=" + RETRY_CRON)
                .routeId("mapping_retry_scheduler")
                .log(LoggingLevel.DEBUG, log, "Retry Mapping Check errors")
                .bean(clinicIQCheckService, "findAllErrors")
                .split(body())
                .bean(checkMappingService, "map")
                .bean(evotorCheckService, "save")
                .end()
                .log(LoggingLevel.DEBUG, log, "Done trying Mapping Check errors");

        // @formatter:on
    }
}