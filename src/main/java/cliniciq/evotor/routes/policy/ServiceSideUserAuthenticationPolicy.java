package cliniciq.evotor.routes.policy;

import cliniciq.evotor.db.service.rest.EvotorSideServiceUserService;
import cliniciq.evotor.utils.exceptions.evotor.EvotorAuthenticationException;
import org.apache.camel.Exchange;
import org.apache.camel.Route;
import org.apache.camel.support.RoutePolicySupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;

import static cliniciq.evotor.utils.enums.HTTPHeaderDictionary.AUTHORIZATION;

@Component
public class ServiceSideUserAuthenticationPolicy extends RoutePolicySupport {
    private final EvotorSideServiceUserService evotorSideServiceUserService;

    @Autowired
    public ServiceSideUserAuthenticationPolicy(EvotorSideServiceUserService evotorSideServiceUserService) {
        this.evotorSideServiceUserService = evotorSideServiceUserService;
    }

    public void onExchangeBegin(Route route, Exchange exchange) {
        String authorizationSideServiceUserToken = Optional.ofNullable(exchange.getIn().getHeader(AUTHORIZATION))
                .map(Object::toString).orElse(null);

        if (Objects.isNull(authorizationSideServiceUserToken)) {
            exchange.setException(new EvotorAuthenticationException("Side Service User Authorization token not provided"));
            return;
        }

        boolean isActiveToken = evotorSideServiceUserService.findAll().stream()
                .anyMatch(evotorSideServiceUser -> authorizationSideServiceUserToken.equals(evotorSideServiceUser.getToken()));

        if (!isActiveToken) {
            exchange.setException(new EvotorAuthenticationException("Side Service User Authorization token is not valid"));
        }
    }
}
