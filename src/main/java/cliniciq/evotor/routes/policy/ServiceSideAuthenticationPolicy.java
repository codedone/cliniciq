package cliniciq.evotor.routes.policy;

import cliniciq.evotor.db.service.rest.SideServiceTokenService;
import cliniciq.evotor.utils.exceptions.evotor.EvotorAuthenticationException;
import org.apache.camel.Exchange;
import org.apache.camel.Route;
import org.apache.camel.support.RoutePolicySupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static cliniciq.evotor.utils.enums.HTTPHeaderDictionary.AUTHORIZATION;

@Component
public class ServiceSideAuthenticationPolicy extends RoutePolicySupport {
    private final SideServiceTokenService sideServiceTokenService;

    @Autowired
    public ServiceSideAuthenticationPolicy(SideServiceTokenService sideServiceTokenService) {
        this.sideServiceTokenService = sideServiceTokenService;
    }

    public void onExchangeBegin(Route route, Exchange exchange) {
        String authorizationToken = Optional.ofNullable(exchange.getIn().getHeader(AUTHORIZATION))
                .map(Object::toString).orElse(null);

        if (!sideServiceTokenService.validateToken(authorizationToken)) {
            exchange.setException(new EvotorAuthenticationException("Cannot authorize request"));
        }
    }
}
