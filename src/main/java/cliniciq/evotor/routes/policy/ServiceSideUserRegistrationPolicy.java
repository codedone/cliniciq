package cliniciq.evotor.routes.policy;

import cliniciq.evotor.utils.exceptions.evotor.EvotorSideServiceUserAuthenticationException;
import org.apache.camel.Exchange;
import org.apache.camel.Route;
import org.apache.camel.support.RoutePolicySupport;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static cliniciq.evotor.utils.enums.HTTPHeaderDictionary.X_EVOTOR_STORE_UUID;

@Component
public class ServiceSideUserRegistrationPolicy extends RoutePolicySupport {

    public void onExchangeBegin(Route route, Exchange exchange) {
        String shopId = exchange.getIn().getHeader(X_EVOTOR_STORE_UUID, String.class);

        if (Objects.isNull(shopId) || shopId.isEmpty()) {
            exchange.setException(new EvotorSideServiceUserAuthenticationException(X_EVOTOR_STORE_UUID + " not set"));
        }
    }
}
