package cliniciq.evotor.routes.evotor;

import cliniciq.evotor.config.KafkaRouteConfiguration;
import cliniciq.evotor.db.domain.cashbox.EvotorPosUser;
import cliniciq.evotor.db.domain.cashbox.EvotorService;
import cliniciq.evotor.db.domain.cashbox.EvotorShop;
import cliniciq.evotor.db.domain.cashbox.EvotorUser;
import cliniciq.evotor.db.service.cashbox.EvotorPosUserService;
import cliniciq.evotor.db.service.cashbox.EvotorServiceService;
import cliniciq.evotor.db.service.cashbox.EvotorShopService;
import cliniciq.evotor.db.service.cashbox.EvotorUserService;
import cliniciq.evotor.service.NotificationService;
import cliniciq.evotor.utils.camel.ArrayListAggregationStrategy;
import cliniciq.evotor.utils.camel.KafkaCamelUtils;
import cliniciq.evotor.utils.exceptions.db.ShopNotFoundDBException;
import org.apache.camel.ExchangeProperty;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.jsonpath.JsonPath;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static cliniciq.evotor.utils.enums.KafkaRouteType.CONSUMER;

@Profile("!no_kafka")
@Component
public class NotificationsConsumerRoute extends RouteBuilder {
    private final static String NOTIFICATION_ROUTE = "seda:notify";

    private final EvotorShopService evotorShopService;
    private final EvotorUserService evotorUserService;
    private final EvotorServiceService evotorServiceService;
    private final EvotorPosUserService evotorPosUserService;
    private final NotificationService notificationService;
    private final KafkaRouteConfiguration kafkaRouteConfiguration;

    @Autowired
    public NotificationsConsumerRoute(EvotorShopService evotorShopService,
                                      EvotorUserService evotorUserService,
                                      EvotorServiceService evotorServiceService,
                                      EvotorPosUserService evotorPosUserService,
                                      NotificationService notificationService,
                                      KafkaRouteConfiguration kafkaRouteConfiguration) {
        this.evotorShopService = evotorShopService;
        this.evotorUserService = evotorUserService;
        this.evotorServiceService = evotorServiceService;
        this.evotorPosUserService = evotorPosUserService;
        this.notificationService = notificationService;
        this.kafkaRouteConfiguration = kafkaRouteConfiguration;
    }

    @Bean
    private AggregationStrategy batchAggregationStrategy() {
        return new ArrayListAggregationStrategy();
    }


    @Override
    public void configure() {
        // @formatter:off

        //TODO add error handling

        // -----------------------------------------------------------//
        from(kafkaRouteConfiguration.servicesTopic(CONSUMER))
                .routeId("evotor_goods_dsystem_kafka_consumer")
                .to("seda:evotor_goods_dsystem_kafka_consumer_processor")
                .process(KafkaCamelUtils::manualCommit);

        from("seda:evotor_goods_dsystem_kafka_consumer_processor")
                .log("${body}")
                .wireTap(NOTIFICATION_ROUTE)
                .process(exchange -> exchange.setProperty("storeUUID", new String((byte[]) exchange.getIn().getHeader("storeUUID"))))
                .split(jsonpath("$"), batchAggregationStrategy())
                .bean(NotificationsConsumerRoute.class, "saveEvotorService")
                .end()
                .setId("evotor_goods_dsystem_kafka_consumer_processor");

        // -----------------------------------------------------------//
        from(kafkaRouteConfiguration.posTopic(CONSUMER))
                .routeId("evotor_pos_dsystem_kafka_consumer")
                .to("seda:evotor_pos_dsystem_kafka_consumer_processor")
                .process(KafkaCamelUtils::manualCommit);

        from("seda:evotor_pos_dsystem_kafka_consumer_processor")
                .log("${body}")
                .wireTap(NOTIFICATION_ROUTE)
                .split(jsonpath("$"), batchAggregationStrategy())
                .bean(NotificationsConsumerRoute.class, "saveEvotorPosUser")
                .end()
                .setId("evotor_pos_dsystem_kafka_consumer_processor");

        // -----------------------------------------------------------//
        from(kafkaRouteConfiguration.userTopic(CONSUMER))
                .routeId("evotor_user_dsystem_kafka_consumer")
                .to("seda:evotor_user_dsystem_kafka_consumer_processor")
                .process(KafkaCamelUtils::manualCommit);

        from("seda:evotor_user_dsystem_kafka_consumer_processor")
                .log("${body}")
                .wireTap(NOTIFICATION_ROUTE)
                .split(jsonpath("$"), batchAggregationStrategy())
                .bean(NotificationsConsumerRoute.class, "saveEvotorUser")
                .end()
                .setId("evotor_user_dsystem_kafka_consumer_processor");

        // -----------------------------------------------------------//
        from(kafkaRouteConfiguration.shopTopic(CONSUMER))
                .routeId("evotor_store_dsystem_kafka_consumer")
                .to("seda:evotor_store_dsystem_kafka_consumer_processor")
                .process(KafkaCamelUtils::manualCommit);

        from("seda:evotor_store_dsystem_kafka_consumer_processor")
                .log("${body}")
                .wireTap(NOTIFICATION_ROUTE)
                .split(jsonpath("$"), batchAggregationStrategy())
                .bean(NotificationsConsumerRoute.class, "saveEvotorStore")
                .end()
                .setId("evotor_store_dsystem_kafka_consumer_processor");

        // -----------------------------------------------------------//
        from(kafkaRouteConfiguration.receiptsTopic(CONSUMER))
                .routeId("evotor_prepared_check_dsystem_kafka_consumer")
                .to("seda:evotor_prepared_check_dsystem_kafka_consumer_processor")
                .process(KafkaCamelUtils::manualCommit);

        from("seda:evotor_prepared_check_dsystem_kafka_consumer_processor")
                .log("${body}")
                .wireTap(NOTIFICATION_ROUTE)
                .end()
                .setId("evotor_prepared_check_dsystem_kafka_consumer_processor");

        // -----------------------------------------------------------//
        from(NOTIFICATION_ROUTE)
                .bean(notificationService, "notifyAdministrator");
        // @formatter:on
    }

    @Transactional
    public EvotorShop saveEvotorStore(@JsonPath("$.uuid") String uuid) {
        EvotorShop existingEvotorShop = evotorShopService.findByEvotorShopUUID(uuid);
        if (Objects.nonNull(existingEvotorShop)) {
            return existingEvotorShop;
        } else {
            return evotorShopService.save(new EvotorShop().setEvotorUUID(uuid));
        }
    }

    @Transactional
    public EvotorUser saveEvotorUser(@JsonPath("$.uuid") String uuid,
                                     @JsonPath("$.stores[*].storeUuid") List<String> stores) {

        List<EvotorShop> receivedExistingShops = stores.stream()
                .map(evotorShopService::findByEvotorShopUUID)
                .collect(Collectors.toList());

        final EvotorUser evotorUser = Optional.ofNullable(evotorUserService.findByEvotorUserUUID(uuid))
                .orElse(evotorUserService.save(new EvotorUser().setEvotorUserUUID(uuid)));

        receivedExistingShops.forEach(data -> data.getEvotorUsers().add(evotorUser));
        evotorShopService.saveAll(receivedExistingShops);
        return evotorUser;
    }

    @Transactional
    public EvotorService saveEvotorService(@JsonPath("$.uuid") String uuid,
                                           @JsonPath("$.measureName") String measureName,
                                           @JsonPath("$.type") String type,
                                           @ExchangeProperty("storeUUID") String storeUUID) {

        EvotorShop receivedExistingShop = evotorShopService.findByEvotorShopUUID(storeUUID);
        if (Objects.isNull(receivedExistingShop)) {
            throw new ShopNotFoundDBException("Shop with uuid not found :" + uuid);
        }

        return evotorServiceService.save(new EvotorService()
                .setEvotorServiceUUID(uuid)
                .setMeasureName(measureName)
                .setType(type)
                .setEvotorShop(receivedExistingShop));
    }

    @Transactional
    public EvotorPosUser saveEvotorPosUser(@JsonPath("$.uuid") String uuid,
                                           @JsonPath("$.storeUuid") String storeUUID) {

        EvotorShop receivedExistingShop = evotorShopService.findByEvotorShopUUID(storeUUID);
        if (Objects.isNull(receivedExistingShop)) {
            throw new ShopNotFoundDBException("Shop with uuid not found :" + uuid);
        }

        EvotorPosUser existingPosUser = evotorPosUserService.findByEvotorDeviceId(uuid);
        if (Objects.nonNull(existingPosUser)) {
            return evotorPosUserService.save(existingPosUser.setEvotorShop(receivedExistingShop));
        } else {
            return evotorPosUserService.save(new EvotorPosUser()
                    .setEvotorDeviceId(uuid)
                    .setEvotorShop(receivedExistingShop));
        }
    }
}
