package cliniciq.evotor.routes.evotor;

import cliniciq.evotor.config.evotor.SideServiceUserRouteConfiguration;
import cliniciq.evotor.db.domain.cashbox.EvotorShop;
import cliniciq.evotor.db.domain.cashbox.EvotorUser;
import cliniciq.evotor.db.domain.rest.EvotorErrorMessage;
import cliniciq.evotor.db.domain.rest.EvotorSideServiceUser;
import cliniciq.evotor.db.service.cashbox.EvotorShopService;
import cliniciq.evotor.db.service.cashbox.EvotorUserService;
import cliniciq.evotor.db.service.rest.EvotorSideServiceUserService;
import cliniciq.evotor.routes.policy.ServiceSideAuthenticationPolicy;
import cliniciq.evotor.utils.exceptions.base.EvotorHttpExceptionBase;
import cliniciq.evotor.utils.exceptions.evotor.EvotorSideServiceUserAssociationException;
import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Component
public class SideServiceUserCreationRoute extends RouteBuilder {
    private static final String EVOTOR_SIDE_SERVICE_USER_CREATION_ROUTE = "seda:evotor_side_service_user_creation_route";
    private static final String ORIGINAL_REQUEST_KEY = "originalRequest";
    private static final String EVOTOR_HTTP_ERROR_STATUS_CODE = "evotorHttpErrorStatusCode";

    private final ServiceSideAuthenticationPolicy serviceSideAuthenticationPolicy;
    private final SideServiceUserRouteConfiguration sideServiceUserRouteConfiguration;
    private final EvotorSideServiceUserService evotorSideServiceUserService;
    private final EvotorShopService evotorShopService;
    private final EvotorUserService evotorUserService;

    @Autowired
    public SideServiceUserCreationRoute(ServiceSideAuthenticationPolicy serviceSideAuthenticationPolicy,
                                        SideServiceUserRouteConfiguration sideServiceUserRouteConfiguration,
                                        EvotorSideServiceUserService evotorSideServiceUserService,
                                        EvotorShopService evotorShopService,
                                        EvotorUserService evotorUserService) {
        this.serviceSideAuthenticationPolicy = serviceSideAuthenticationPolicy;
        this.sideServiceUserRouteConfiguration = sideServiceUserRouteConfiguration;
        this.evotorSideServiceUserService = evotorSideServiceUserService;
        this.evotorShopService = evotorShopService;
        this.evotorUserService = evotorUserService;
    }


    @Override
    public void configure() {
        // @formatter:off

        onException(EvotorHttpExceptionBase.class)
                .handled(true)
                .log(LoggingLevel.WARN, log, "EvotorHttpExceptionBase occurred in SideServiceUserCreationRoute")
                .log(LoggingLevel.WARN, log, "${exception.message}")
                .process(exchange -> {
                    EvotorHttpExceptionBase exception = (EvotorHttpExceptionBase) exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
                    exchange.setProperty(EVOTOR_HTTP_ERROR_STATUS_CODE, exception.getErrorCode());
                    exchange.getIn().setBody(EvotorErrorMessage.of(exception.getEvotorErrorCode()));
                })
                .marshal().json(JsonLibrary.Jackson)
                .setHeader(Exchange.HTTP_RESPONSE_CODE, simple("${property." + EVOTOR_HTTP_ERROR_STATUS_CODE + "}"));

        onException(Exception.class)
                .handled(true)
                .log(LoggingLevel.ERROR, log, "Exception occurred in EvotorSideServiceUserCreationRoute")
                .log(LoggingLevel.ERROR, log, "${exception.message}")
                .setBody(constant(""))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.INTERNAL_SERVER_ERROR.value()));

        from(sideServiceUserRouteConfiguration.creationPath())
                .description("Evotor Side Service User creation processor")
                .routeId("evotor_side_service_user_creation_processor")
                .routePolicy(serviceSideAuthenticationPolicy)
                .setProperty(ORIGINAL_REQUEST_KEY, simple("${body}"))
                .log("Received from Evotor User Creation >>> ${body}")
                .log("Received from Evotor User Creation >>> ${headers}")
                .unmarshal().json(JsonLibrary.Jackson, EvotorSideServiceUser.class)
                .to(EVOTOR_SIDE_SERVICE_USER_CREATION_ROUTE)
                .marshal().json(JsonLibrary.Jackson)
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200));

        from(EVOTOR_SIDE_SERVICE_USER_CREATION_ROUTE)
                .description("ClinicIQ user Creation service Call")
                .routeId("cliniciq_user_creation_service_call")
                .bean(SideServiceUserCreationRoute.class, "saveSideServiceUser")
                .end();

        // @formatter:on
    }


    // TODO refactor
    public EvotorSideServiceUser saveSideServiceUser(@Header(value = "X-Evotor-Store-Uuid") String shopUUID,
                                                     @Body EvotorSideServiceUser evotorSideServiceUser) {

        EvotorShop evotorShop = evotorShopService.findByEvotorShopUUID(shopUUID);
        if (Objects.isNull(evotorShop)) {
            evotorShop = evotorShopService.save(new EvotorShop().setEvotorUUID(shopUUID));
        }

        EvotorUser evotorUser = evotorUserService.findByEvotorUserUUIDAndEvotorShopsEvotorUUID(evotorSideServiceUser.getUserId(), shopUUID);
        if (Objects.nonNull(evotorUser)) {
            throw new EvotorSideServiceUserAssociationException("Provided user already registered in this shop");
        } else {
            evotorUser = Optional.ofNullable(evotorUserService.findByEvotorUserUUID(evotorSideServiceUser.getUserId()))
                    .orElseGet(() -> evotorUserService.save(new EvotorUser().setEvotorUserUUID(evotorSideServiceUser.getUserId())));
            evotorShop.getEvotorUsers().add(evotorUser);
            evotorShopService.save(evotorShop);
        }

        return evotorSideServiceUserService.save(evotorSideServiceUser.setShopId(shopUUID).setToken(UUID.randomUUID().toString()));
    }
}
