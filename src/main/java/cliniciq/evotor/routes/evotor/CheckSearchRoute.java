package cliniciq.evotor.routes.evotor;

import cliniciq.evotor.config.evotor.CheckSearchRouteConfiguration;
import cliniciq.evotor.db.domain.enums.CheckStatus;
import cliniciq.evotor.db.domain.rest.EvotorCheck;
import cliniciq.evotor.db.service.rest.EvotorCheckService;
import cliniciq.evotor.utils.exceptions.db.CheckFoundDBException;
import com.dsystem.utils.rest.wrapper.ResultDataWrapper;
import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

import static cliniciq.evotor.utils.enums.HTTPHeaderDictionary.*;

@Component
public class CheckSearchRoute extends RouteBuilder {
    private final EvotorCheckService evotorCheckService;
    private final CheckSearchRouteConfiguration checkSearchRouteConfiguration;

    @Autowired
    public CheckSearchRoute(EvotorCheckService evotorCheckService,
                            CheckSearchRouteConfiguration checkSearchRouteConfiguration) {
        this.evotorCheckService = evotorCheckService;
        this.checkSearchRouteConfiguration = checkSearchRouteConfiguration;
    }

    @Override
    public void configure() {
        // @formatter:off

        onException(CheckFoundDBException.class)
                .handled(true)
                .log(LoggingLevel.WARN, log, "EvotorHttpExceptionBase occurred in CheckSearchRoute")
                .log(LoggingLevel.WARN, log, "${exception.message}")
                .process(exchange -> {
                    CheckFoundDBException exception = (CheckFoundDBException) exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
                    ResultDataWrapper resultDataWrapper = new ResultDataWrapper();
                    resultDataWrapper.setResultCode(1);
                    resultDataWrapper.setResultMessage(exception.getMessage());
                    exchange.getIn().setBody(resultDataWrapper);
                })
                .marshal().json(JsonLibrary.Jackson)
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(500));

        from(checkSearchRouteConfiguration.checkSearchPath())
                .routeId("evotor_check_search_processor")
                .log("${body}")
                .bean(CheckSearchRoute.class, "find")
                .bean(CheckSearchRoute.class, "markAsSend")
                .bean(CheckSearchRoute.class, "extractJson")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200));
        // @formatter:on
    }

    public EvotorCheck find(@Header(X_EVOTOR_STORE_UUID) String storeId, @Header(X_EVOTOR_USER_ID) String userId,
                            @Header(X_EVOTOR_DEVICE_UUID) String deviceId) {

        if (Objects.isNull(storeId) || Objects.isNull(deviceId)) {
            throw new CheckFoundDBException("Store id or Device id not provided");
        }

        List<EvotorCheck> evotorChecks;
        if (Objects.isNull(userId)) {
            evotorChecks = evotorCheckService.findProcessedByShopUUIDAndDeviceUUID(storeId, deviceId);
        } else {
            evotorChecks = evotorCheckService.findProcessedByShopUUIDAndUserUUIDAndDeviceUUID(storeId, userId, deviceId);
        }

        return evotorChecks.stream().findFirst().orElseThrow(() -> new CheckFoundDBException("Cannot find payment with parameters: " + storeId + ", :" + userId));
    }

    public EvotorCheck markAsSend(@NotNull EvotorCheck evotorCheck) {
        return evotorCheckService.save(evotorCheck.setCheckStatus(CheckStatus.SEND));
    }

    public String extractJson(@NotNull EvotorCheck evotorCheck) {
        return evotorCheck.getCheckJson();
    }
}
