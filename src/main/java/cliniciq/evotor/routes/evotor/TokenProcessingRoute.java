package cliniciq.evotor.routes.evotor;

import cliniciq.evotor.config.evotor.TokenProcessorRouteConfiguration;
import cliniciq.evotor.db.domain.rest.EvotorErrorCode;
import cliniciq.evotor.db.domain.rest.EvotorErrorMessage;
import cliniciq.evotor.db.domain.rest.EvotorToken;
import cliniciq.evotor.db.service.rest.EvotorTokenService;
import cliniciq.evotor.routes.policy.ServiceSideAuthenticationPolicy;
import cliniciq.evotor.utils.exceptions.evotor.EvotorAuthenticationException;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.jsonpath.JsonPath;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class TokenProcessingRoute extends RouteBuilder {
    private static final String ORIGINAL_REQUEST_KEY = "originalRequest";

    private final EvotorTokenService evotorTokenService;
    private final ServiceSideAuthenticationPolicy serviceSideAuthenticationPolicy;
    private final TokenProcessorRouteConfiguration tokenProcessorRouteConfiguration;

    @Autowired
    public TokenProcessingRoute(EvotorTokenService evotorTokenService,
                                ServiceSideAuthenticationPolicy serviceSideAuthenticationPolicy,
                                TokenProcessorRouteConfiguration tokenProcessorRouteConfiguration) {
        this.evotorTokenService = evotorTokenService;
        this.serviceSideAuthenticationPolicy = serviceSideAuthenticationPolicy;
        this.tokenProcessorRouteConfiguration = tokenProcessorRouteConfiguration;
    }

    @Override
    public void configure() {
        // @formatter:off

        onException(EvotorAuthenticationException.class)
                .handled(true)
                .log(LoggingLevel.WARN, log, "AuthenticationException occurred in EvotorTokenRoute")
                .log(LoggingLevel.WARN, log, "${exception.message}")
                .setBody(constant(EvotorErrorMessage.of(EvotorErrorCode.INCORRECT_EVOTOR_TOKEN)))
                .marshal().json(JsonLibrary.Jackson)
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.UNAUTHORIZED.value()));

        onException(Exception.class)
                .handled(true)
                .log(LoggingLevel.ERROR, log, "Exception occurred in EvotorTokenRoute")
                .log(LoggingLevel.ERROR, log, "${exception.message}")
                .setBody(simple("${property." + ORIGINAL_REQUEST_KEY + "}"))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.INTERNAL_SERVER_ERROR.value()));

        from(tokenProcessorRouteConfiguration.authPath())
                .description("Evotor Token processor")
                .routeId("evotor_token_processor")
                .routePolicy(serviceSideAuthenticationPolicy)
                .setProperty(ORIGINAL_REQUEST_KEY, simple("${body}"))
                .log("Received from Evotor >>> ${body}")
                .bean(TokenProcessingRoute.class, "transformToEvotorToken")
                .bean(evotorTokenService, "save")
                .setBody(exchangeProperty("originalRequest"))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200));

        // @formatter:on
    }

    public EvotorToken transformToEvotorToken(@JsonPath("$.token") String token) {
        return new EvotorToken().setToken(token);
    }
}
