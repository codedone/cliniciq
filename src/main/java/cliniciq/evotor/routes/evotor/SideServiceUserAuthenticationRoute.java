package cliniciq.evotor.routes.evotor;

import cliniciq.evotor.config.evotor.LogRouteConfiguration;
import cliniciq.evotor.config.evotor.SideServiceUserRouteConfiguration;
import cliniciq.evotor.db.domain.cashbox.EvotorShop;
import cliniciq.evotor.db.domain.cashbox.EvotorUser;
import cliniciq.evotor.db.domain.rest.EvotorErrorMessage;
import cliniciq.evotor.db.domain.rest.EvotorSideServiceUser;
import cliniciq.evotor.db.service.cashbox.EvotorShopService;
import cliniciq.evotor.db.service.cashbox.EvotorUserService;
import cliniciq.evotor.db.service.rest.EvotorSideServiceUserService;
import cliniciq.evotor.routes.policy.ServiceSideAuthenticationPolicy;
import cliniciq.evotor.routes.policy.ServiceSideUserRegistrationPolicy;
import cliniciq.evotor.utils.exceptions.base.EvotorHttpExceptionBase;
import cliniciq.evotor.utils.exceptions.evotor.EvotorSideServiceUserAssociationException;
import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.UUID;

@Component
public class SideServiceUserAuthenticationRoute extends RouteBuilder {
    private static final String EVOTOR_SIDE_SERVICE_USER_AUTHENTICATION_ROUTE = "seda:evotor_side_service_user_authentication_route";
    private static final String ORIGINAL_REQUEST_KEY = "originalRequest";
    private static final String EVOTOR_HTTP_ERROR_STATUS_CODE = "evotorHttpErrorStatusCode";

    private final ServiceSideUserRegistrationPolicy serviceSideUserRegistrationPolicy;
    private final ServiceSideAuthenticationPolicy serviceSideAuthenticationPolicy;
    private final SideServiceUserRouteConfiguration sideServiceUserRouteConfiguration;
    private final EvotorSideServiceUserService evotorSideServiceUserService;
    private final EvotorShopService evotorShopService;
    private final EvotorUserService evotorUserService;
    private final LogRouteConfiguration logRouteConfiguration;

    @Autowired
    public SideServiceUserAuthenticationRoute(ServiceSideUserRegistrationPolicy serviceSideUserRegistrationPolicy, ServiceSideAuthenticationPolicy serviceSideAuthenticationPolicy, SideServiceUserRouteConfiguration sideServiceUserRouteConfiguration, EvotorSideServiceUserService evotorSideServiceUserService, EvotorShopService evotorShopService, EvotorUserService evotorUserService, LogRouteConfiguration logRouteConfiguration) {
        this.serviceSideUserRegistrationPolicy = serviceSideUserRegistrationPolicy;
        this.serviceSideAuthenticationPolicy = serviceSideAuthenticationPolicy;
        this.sideServiceUserRouteConfiguration = sideServiceUserRouteConfiguration;
        this.evotorSideServiceUserService = evotorSideServiceUserService;
        this.evotorShopService = evotorShopService;
        this.evotorUserService = evotorUserService;
        this.logRouteConfiguration = logRouteConfiguration;
    }


    @Override
    public void configure() {
        // @formatter:off

        onException(EvotorHttpExceptionBase.class)
                .handled(true)
                .log(LoggingLevel.WARN, log, "EvotorHttpExceptionBase occurred in EvotorSideServiceUserAuthenticationRoute")
                .log(LoggingLevel.WARN, log, "${exception.message}")
                .process(exchange -> {
                    EvotorHttpExceptionBase exception = (EvotorHttpExceptionBase) exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
                    exchange.setProperty(EVOTOR_HTTP_ERROR_STATUS_CODE, exception.getErrorCode());
                    exchange.getIn().setBody(EvotorErrorMessage.of(exception.getEvotorErrorCode()));
                })
                .marshal().json(JsonLibrary.Jackson)
                .setHeader(Exchange.HTTP_RESPONSE_CODE, simple("${property." + EVOTOR_HTTP_ERROR_STATUS_CODE + "}"));

        onException(Exception.class)
                .handled(true)
                .log(LoggingLevel.ERROR, log, "Exception occurred in EvotorSideServiceUserAuthenticationRoute")
                .log(LoggingLevel.ERROR, log, "${exception.message}")
                .setBody(constant(""))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.INTERNAL_SERVER_ERROR.value()));

        from(sideServiceUserRouteConfiguration.authPath())
                .description("Evotor Side Service User Auth processor")
                .routeId("evotor_side_service_user_auth_processor")
                .routePolicy(serviceSideAuthenticationPolicy)
                .wireTap(logRouteConfiguration.inLogPath())
                .setProperty(ORIGINAL_REQUEST_KEY, simple("${body}"))
                .log("Received from Evotor User Authentication >>> ${body}")
                .log("Received from Evotor User Authentication >>> ${headers}")
                .unmarshal().json(JsonLibrary.Jackson, EvotorSideServiceUser.class)
                .to(EVOTOR_SIDE_SERVICE_USER_AUTHENTICATION_ROUTE)
                .marshal().json(JsonLibrary.Jackson)
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200));

        from(EVOTOR_SIDE_SERVICE_USER_AUTHENTICATION_ROUTE)
                .description("ClinicIQ user Authentication service Call")
                .routeId("cliniciq_user_authentication_service_call")
                .routePolicy(serviceSideUserRegistrationPolicy)
                .bean(SideServiceUserAuthenticationRoute.class, "mapEvotorSideServiceUser")
                .bean(SideServiceUserAuthenticationRoute.class, "findActiveAuthentication")
                .choice()
                .when(simple("${body.id} == null"))
                .bean(SideServiceUserAuthenticationRoute.class, "generateToken")
                .bean(evotorSideServiceUserService, "save")
                .end();


        // @formatter:on
    }

    public EvotorSideServiceUser mapEvotorSideServiceUser(@Header(value = "X-Evotor-Store-Uuid") String shopUUID,
                                                          @Body EvotorSideServiceUser evotorSideServiceUser) {

        EvotorShop evotorShop = evotorShopService.findByEvotorShopUUID(shopUUID);
        if (Objects.isNull(evotorShop)) {
            throw new EvotorSideServiceUserAssociationException("Provided shop not found");
        }

        EvotorUser evotorUser = evotorUserService.findByEvotorUserUUIDAndEvotorShopsEvotorUUID(evotorSideServiceUser.getUserId(), shopUUID);
        if (Objects.isNull(evotorUser)) {
            throw new EvotorSideServiceUserAssociationException("Provided user not found");
        }

        return evotorSideServiceUser.setShopId(shopUUID);
    }

    public EvotorSideServiceUser generateToken(@Body EvotorSideServiceUser evotorSideServiceUser) {
        return evotorSideServiceUser.setToken(UUID.randomUUID().toString());
    }


    public EvotorSideServiceUser findActiveAuthentication(@Body EvotorSideServiceUser evotorSideServiceUser) {
        EvotorSideServiceUser existing = evotorSideServiceUserService.findByUserIdAndShopId(evotorSideServiceUser.getUserId(),
                evotorSideServiceUser.getShopId());

        if (Objects.isNull(existing)) {
            return evotorSideServiceUser;
        }

        return existing;
    }
}
