package cliniciq.evotor.routes.evotor;

import cliniciq.evotor.config.KafkaRouteConfiguration;
import cliniciq.evotor.config.evotor.NotificationRouteConfiguration;
import cliniciq.evotor.db.domain.rest.EvotorErrorCode;
import cliniciq.evotor.db.domain.rest.EvotorErrorMessage;
import cliniciq.evotor.routes.policy.ServiceSideUserAuthenticationPolicy;
import cliniciq.evotor.utils.exceptions.evotor.EvotorAuthenticationException;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import static cliniciq.evotor.utils.enums.KafkaRouteType.PRODUCER;

@Profile("!no_kafka")
@Component
public class NotificationsProducerRoute extends RouteBuilder {
    private final ServiceSideUserAuthenticationPolicy serviceSideUserAuthenticationPolicy;
    private final KafkaRouteConfiguration kafkaRouteConfiguration;
    private final NotificationRouteConfiguration notificationRouteConfiguration;


    @Autowired
    public NotificationsProducerRoute(ServiceSideUserAuthenticationPolicy serviceSideUserAuthenticationPolicy,
                                      KafkaRouteConfiguration kafkaRouteConfiguration,
                                      NotificationRouteConfiguration notificationRouteConfiguration) {
        this.serviceSideUserAuthenticationPolicy = serviceSideUserAuthenticationPolicy;
        this.kafkaRouteConfiguration = kafkaRouteConfiguration;
        this.notificationRouteConfiguration = notificationRouteConfiguration;
    }

    @Override
    public void configure() {
        // @formatter:off

        onException(EvotorAuthenticationException.class)
                .handled(true)
                .log(LoggingLevel.WARN, log, "EvotorHttpExceptionBase occurred in EvotorNotificationsKafkaProducerRoute")
                .log(LoggingLevel.WARN, log, "${exception.message}")
                .setBody(constant(EvotorErrorMessage.of(EvotorErrorCode.INCORRECT_EVOTOR_USER_TOKEN)))
                .marshal().json(JsonLibrary.Jackson)
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(401));

        onException(Exception.class)
                .handled(true)
                .log(LoggingLevel.ERROR, log, "Exception occurred in EvotorSideServiceUserAuthenticationRoute")
                .log(LoggingLevel.ERROR, log, "${exception.message}")
                .setBody(constant(""))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.INTERNAL_SERVER_ERROR.value()));

        // -----------------------------------------------------------//
        from(notificationRouteConfiguration.servicesPath())
                .routeId("send_evotor_goods_dsystem_to_kafka")
                .routePolicy(serviceSideUserAuthenticationPolicy)
                .removeHeaders("*")
                .to(kafkaRouteConfiguration.servicesTopic(PRODUCER))
                .setBody(constant(""))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200));

        // -----------------------------------------------------------//
        from(notificationRouteConfiguration.posPath())
                .routeId("send_evotor_pos_dsystem_to_kafka")
                .routePolicy(serviceSideUserAuthenticationPolicy)
                .removeHeaders("*")
                .to(kafkaRouteConfiguration.posTopic(PRODUCER))
                .setBody(constant(""))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200));

        // -----------------------------------------------------------//
        from(notificationRouteConfiguration.userPath())
                .routeId("send_evotor_user_dsystem_to_kafka")
                .routePolicy(serviceSideUserAuthenticationPolicy)
                .removeHeaders("*")
                .to(kafkaRouteConfiguration.userTopic(PRODUCER))
                .setBody(constant(""))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200));

        // -----------------------------------------------------------//
        from(notificationRouteConfiguration.shopPath())
                .routeId("send_evotor_shop_dsystem_to_kafka")
                .routePolicy(serviceSideUserAuthenticationPolicy)
                .removeHeaders("*")
                .to(kafkaRouteConfiguration.shopTopic(PRODUCER))
                .setBody(constant(""))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200));

        // -----------------------------------------------------------//
        from(notificationRouteConfiguration.checkPath())
                .routeId("send_evotor_prepared_check_dsystem_to_kafka")
                .routePolicy(serviceSideUserAuthenticationPolicy)
                .removeHeaders("*")
                .to(kafkaRouteConfiguration.receiptsTopic(PRODUCER))
                .setBody(constant(""))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200));

        // @formatter:on
    }
}
