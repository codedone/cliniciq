package cliniciq.evotor.routes.components;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaComponent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("!no_kafka")
public class KafkaCamelComponent extends RouteBuilder {

    @Value("${service.kafka.url}")
    String kafkaUrl;

    @Value("${service.kafka.port}")
    String kafkaPort;

    @Override
    public void configure() {
        KafkaComponent kafka = new KafkaComponent();
        kafka.setBrokers(kafkaUrl + ":" + kafkaPort);
        kafka.setAllowManualCommit(true);
        getContext().addComponent("kafka", kafka);
    }
}
