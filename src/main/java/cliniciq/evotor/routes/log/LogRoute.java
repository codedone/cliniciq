package cliniciq.evotor.routes.log;

import cliniciq.evotor.config.evotor.LogRouteConfiguration;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LogRoute extends RouteBuilder {

    @Autowired
    private LogRouteConfiguration logRouteConfiguration;

    @Override
    public void configure() {
        // @formatter:off
        from(logRouteConfiguration.inLogPath())
                .routeId("logRoute")
                .convertBodyTo(String.class)
                .log("In >>>>> ${headers} ${body}")
                .end();
        // @formatter:on
    }

}