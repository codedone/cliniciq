package cliniciq.evotor.db.service.rest;

import cliniciq.evotor.db.domain.enums.CheckStatus;
import cliniciq.evotor.db.domain.rest.EvotorCheck;
import cliniciq.evotor.db.repository.rest.EvotorCheckRepository;
import cliniciq.evotor.utils.exceptions.db.CheckResponseSaveDBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
@Transactional
public class EvotorCheckService {
    private final EvotorCheckRepository evotorCheckRepository;

    @Autowired
    public EvotorCheckService(EvotorCheckRepository evotorCheckRepository) {
        this.evotorCheckRepository = evotorCheckRepository;
    }

    public List<EvotorCheck> findByShopUUIDAndUserUUID(String shopUUID, String userUUID) {
        return evotorCheckRepository.findByShopUUIDAndUserUUID(shopUUID, userUUID);
    }

    public List<EvotorCheck> findProcessedByShopUUIDAndUserUUIDAndDeviceUUID(String shopUUID, String userUUID, String deviceUUID) {
        return evotorCheckRepository.findByShopUUIDAndUserUUIDAndDeviceUUIDAndCheckStatus(shopUUID, userUUID, deviceUUID, CheckStatus.PROCESSED);
    }

    public List<EvotorCheck> findProcessedByShopUUIDAndDeviceUUID(String shopUUID, String deviceUUID) {
        return evotorCheckRepository.findByShopUUIDAndDeviceUUIDAndCheckStatus(shopUUID, deviceUUID, CheckStatus.PROCESSED);
    }

    public List<EvotorCheck> findAll() {
        return evotorCheckRepository.findAll();
    }

    public EvotorCheck save(@NotNull EvotorCheck evotorCheck) {
        try {
            return evotorCheckRepository.save(evotorCheck);
        } catch (Exception e) {
            throw new CheckResponseSaveDBException(e.getMessage());
        }
    }
}
