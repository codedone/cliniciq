package cliniciq.evotor.db.service.rest;

import cliniciq.evotor.db.domain.rest.EvotorToken;
import cliniciq.evotor.db.repository.rest.EvotorTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;

@Service
public class EvotorTokenService {
    public static final String EVOTOR_TOKEN_CACHE_KEY = "activeEvotorToken";

    private final EvotorTokenRepository evotorTokenRepository;

    @Autowired
    public EvotorTokenService(EvotorTokenRepository evotorTokenRepository) {
        this.evotorTokenRepository = evotorTokenRepository;
    }

    @Transactional
    @Cacheable(EVOTOR_TOKEN_CACHE_KEY)
    public EvotorToken findLast() {
        return evotorTokenRepository.findTopByOrderByCreationDateTimeDesc();
    }

    @Transactional
    @CacheEvict(value = EVOTOR_TOKEN_CACHE_KEY, allEntries = true)
    public EvotorToken save(@NotNull EvotorToken token) {
        return evotorTokenRepository.save(token);
    }

    public Boolean validateToken(String receivedToken) {
        return findLast().getToken().equals(receivedToken);
    }
}
