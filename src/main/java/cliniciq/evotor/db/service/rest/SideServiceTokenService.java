package cliniciq.evotor.db.service.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SideServiceTokenService {

    @Value(value = "${evotor.side.service.authentication.token}")
    private String token;

    public Boolean validateToken(String receivedToken) {
        return token.equals(receivedToken);
    }
}
