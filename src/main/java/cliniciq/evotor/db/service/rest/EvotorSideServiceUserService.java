package cliniciq.evotor.db.service.rest;

import cliniciq.evotor.db.domain.rest.EvotorSideServiceUser;
import cliniciq.evotor.db.repository.rest.EvotorSideServiceUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
@Transactional
public class EvotorSideServiceUserService {
    public static final String EVOTOR_SIDE_SERVICE_USER_TOKEN_CACHE_KEY = "activeEvotorSideServiceUserToken";
    private final EvotorSideServiceUserRepository evotorSideServiceUserRepository;

    @Autowired
    public EvotorSideServiceUserService(EvotorSideServiceUserRepository evotorSideServiceUserRepository) {
        this.evotorSideServiceUserRepository = evotorSideServiceUserRepository;
    }

    public EvotorSideServiceUser findByUserIdAndShopId(@NotNull String userId, @NotNull String shopId) {
        return evotorSideServiceUserRepository.findByUserIdAndShopId(userId, shopId);
    }

    @Cacheable(value = EVOTOR_SIDE_SERVICE_USER_TOKEN_CACHE_KEY)
    public List<EvotorSideServiceUser> findAll() {
        return evotorSideServiceUserRepository.findAll();
    }

    @Cacheable(value = EVOTOR_SIDE_SERVICE_USER_TOKEN_CACHE_KEY, key = "#userId")
    public EvotorSideServiceUser findByUserId(@NotNull String userId) {
        return evotorSideServiceUserRepository.findByUserId(userId);
    }

    @CacheEvict(value = EVOTOR_SIDE_SERVICE_USER_TOKEN_CACHE_KEY, key = "#evotorSideServiceUser.getUserId()")
    public void delete(@NotNull EvotorSideServiceUser evotorSideServiceUser) {
        evotorSideServiceUserRepository.delete(evotorSideServiceUser);
    }

    @CacheEvict(value = EVOTOR_SIDE_SERVICE_USER_TOKEN_CACHE_KEY, key = "#evotorSideServiceUser.getUserId()")
    public EvotorSideServiceUser save(@NotNull EvotorSideServiceUser evotorSideServiceUser) {
        return evotorSideServiceUserRepository.save(evotorSideServiceUser);
    }
}
