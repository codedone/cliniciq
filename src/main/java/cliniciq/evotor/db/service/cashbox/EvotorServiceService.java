package cliniciq.evotor.db.service.cashbox;

import cliniciq.evotor.db.domain.cashbox.EvotorService;
import cliniciq.evotor.db.repository.cashbox.EvotorServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
@Transactional
public class EvotorServiceService {
    private final EvotorServiceRepository evotorServiceRepository;

    @Autowired
    public EvotorServiceService(EvotorServiceRepository evotorServiceRepository) {
        this.evotorServiceRepository = evotorServiceRepository;
    }

    public EvotorService findByEvotorServiceUUID(@NotNull String evotorServiceUUID) {
        return evotorServiceRepository.findByEvotorServiceUUID(evotorServiceUUID);
    }

    public EvotorService findByDSystemALTCode(@NotNull String dSystemALTCode) {
        return evotorServiceRepository.findBydSystemALTCode(dSystemALTCode);
    }


    public List<EvotorService> findAll() {
        return evotorServiceRepository.findAll();
    }

    public EvotorService save(@NotNull EvotorService evotorUser) {
        return evotorServiceRepository.save(evotorUser);
    }
}
