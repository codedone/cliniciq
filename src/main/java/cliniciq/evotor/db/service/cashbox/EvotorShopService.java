package cliniciq.evotor.db.service.cashbox;

import cliniciq.evotor.db.domain.cashbox.EvotorShop;
import cliniciq.evotor.db.repository.cashbox.EvotorShopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
@Transactional
public class EvotorShopService {
    private final EvotorShopRepository evotorShopRepository;

    @Autowired
    public EvotorShopService(EvotorShopRepository evotorShopRepository) {
        this.evotorShopRepository = evotorShopRepository;
    }

    public EvotorShop findByEvotorShopUUID(@NotNull String evotorUUID) {
        return evotorShopRepository.findByEvotorUUID(evotorUUID);
    }

    public EvotorShop findByDSystemDomainId(@NotNull Integer dSystemDomainId) {
        return evotorShopRepository.findByDSystemDomainId(dSystemDomainId);
    }

    public EvotorShop findByEvotorUsersDSystemUserId(@NotNull Integer dSystemDomainId) {
        return evotorShopRepository.findByDSystemDomainId(dSystemDomainId);
    }

    public EvotorShop save(@NotNull EvotorShop evotorShop) {
        return evotorShopRepository.save(evotorShop);
    }

    public void saveAll(@NotNull List<EvotorShop> evotorShops) {
        evotorShopRepository.saveAll(evotorShops);
    }

    public List<EvotorShop> findAll() {
        return evotorShopRepository.findAll();
    }
}
