package cliniciq.evotor.db.service.cashbox;

import cliniciq.evotor.db.domain.cashbox.ClinicIQCheck;
import cliniciq.evotor.db.domain.enums.CheckStatus;
import cliniciq.evotor.db.repository.cashbox.ClinicIQCheckRepository;
import cliniciq.evotor.utils.exceptions.db.CheckRequestSaveDBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
@Transactional
public class ClinicIQCheckService {
    private final ClinicIQCheckRepository clinicIQCheckRepository;

    @Autowired
    public ClinicIQCheckService(ClinicIQCheckRepository clinicIQCheckRepository) {
        this.clinicIQCheckRepository = clinicIQCheckRepository;
    }

    public List<ClinicIQCheck> findWithStatusError() {
        return clinicIQCheckRepository.findByCheckStatus(CheckStatus.ERROR);
    }

    public List<ClinicIQCheck> findAllErrors() {
        return clinicIQCheckRepository.findByCheckStatus(CheckStatus.ERROR);
    }

    public ClinicIQCheck save(@NotNull ClinicIQCheck clinicIQCheck) {
        try {
            return clinicIQCheckRepository.save(clinicIQCheck);
        } catch (Exception e) {
            throw new CheckRequestSaveDBException(e.getMessage());
        }
    }

    public void markAsError(@NotNull ClinicIQCheck clinicIQCheck) {
        try {
            clinicIQCheckRepository.save(clinicIQCheck.setCheckStatus(CheckStatus.ERROR));
        } catch (Exception e) {
            throw new CheckRequestSaveDBException(e.getMessage());
        }
    }
}
