package cliniciq.evotor.db.service.cashbox;

import cliniciq.evotor.db.domain.cashbox.EvotorPosUser;
import cliniciq.evotor.db.domain.cashbox.EvotorShop;
import cliniciq.evotor.db.domain.cashbox.EvotorUser;
import cliniciq.evotor.db.repository.cashbox.EvotorPosUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
@Transactional
public class EvotorPosUserService {
    private final EvotorPosUserRepository evotorPosUserRepository;

    @Autowired
    public EvotorPosUserService(EvotorPosUserRepository evotorPosUserRepository) {
        this.evotorPosUserRepository = evotorPosUserRepository;
    }

    public EvotorPosUser findByEvotorDeviceId(@NotNull String evotorUserUUID) {
        return evotorPosUserRepository.findByEvotorDeviceId(evotorUserUUID);
    }

    public EvotorPosUser findByEvotorUserAndEvotorShop(@NotNull EvotorShop evotorShop, @NotNull EvotorUser evotorUser) {
        return evotorPosUserRepository.findByEvotorShopAndEvotorUser(evotorShop, evotorUser);
    }

    public List<EvotorPosUser> findByEvotorShop(@NotNull EvotorShop evotorShop) {
        return evotorPosUserRepository.findByEvotorShop(evotorShop);
    }

    public List<EvotorPosUser> findAll() {
        return evotorPosUserRepository.findAll();
    }

    public void delete(EvotorPosUser evotorPosUser) {
        evotorPosUserRepository.delete(evotorPosUser);
    }

    public EvotorPosUser save(@NotNull EvotorPosUser evotorUser) {
        return evotorPosUserRepository.save(evotorUser);
    }
}
