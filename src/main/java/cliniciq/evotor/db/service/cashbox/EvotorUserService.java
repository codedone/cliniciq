package cliniciq.evotor.db.service.cashbox;

import cliniciq.evotor.db.domain.cashbox.EvotorUser;
import cliniciq.evotor.db.repository.cashbox.EvotorUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
@Transactional
public class EvotorUserService {
    private final EvotorUserRepository evotorUserRepository;

    @Autowired
    public EvotorUserService(EvotorUserRepository evotorUserRepository) {
        this.evotorUserRepository = evotorUserRepository;
    }

    public EvotorUser findByEvotorUserUUID(@NotNull String evotorUserUUID) {
        return evotorUserRepository.findByEvotorUserUUID(evotorUserUUID);
    }

    public EvotorUser findByDSystemUserId(@NotNull Integer dSystemUserId) {
        return evotorUserRepository.findByDSystemUserId(dSystemUserId);
    }

    public EvotorUser findByEvotorUserUUIDAndEvotorShopsEvotorUUID(@NotNull String evotorUserUUID, String evotorUUID) {
        return evotorUserRepository.findByEvotorUserUUIDAndEvotorShops_EvotorUUID(evotorUserUUID, evotorUUID);
    }

    public List<EvotorUser> findAll() {
        return evotorUserRepository.findAll();
    }

    public void delete(@NotNull EvotorUser evotorUser) {
        evotorUserRepository.delete(evotorUser);
    }

    public EvotorUser save(@NotNull EvotorUser evotorUser) {
        return evotorUserRepository.save(evotorUser);
    }
}
