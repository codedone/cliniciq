package cliniciq.evotor.db.repository.rest;

import cliniciq.evotor.db.domain.rest.EvotorToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EvotorTokenRepository extends JpaRepository<EvotorToken, Integer> {
    EvotorToken findTopByOrderByCreationDateTimeDesc();

    <S extends EvotorToken> S save(S token);
}

