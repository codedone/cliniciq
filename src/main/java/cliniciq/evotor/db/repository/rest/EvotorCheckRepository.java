package cliniciq.evotor.db.repository.rest;

import cliniciq.evotor.db.domain.enums.CheckStatus;
import cliniciq.evotor.db.domain.rest.EvotorCheck;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EvotorCheckRepository extends JpaRepository<EvotorCheck, Integer> {

    List<EvotorCheck> findByShopUUIDAndUserUUIDAndDeviceUUIDAndCheckStatus(String shopUUID, String deviceUUID, String userUUID, CheckStatus checkStatus);

    List<EvotorCheck> findByShopUUIDAndDeviceUUIDAndCheckStatus(String shopUUID, String deviceUUID, CheckStatus checkStatus);

    List<EvotorCheck> findByShopUUIDAndUserUUID(String shopUUID, String userUUID);

    List<EvotorCheck> findAll();

    <S extends EvotorCheck> S save(S token);
}

