package cliniciq.evotor.db.repository.rest;

import cliniciq.evotor.db.domain.rest.EvotorSideServiceUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EvotorSideServiceUserRepository extends JpaRepository<EvotorSideServiceUser, Integer> {

    EvotorSideServiceUser findByUserId(String userId);

    EvotorSideServiceUser findByUserIdAndShopId(String userId, String shopId);

    <S extends EvotorSideServiceUser> S save(S token);
}

