package cliniciq.evotor.db.repository.cashbox;

import cliniciq.evotor.db.domain.cashbox.EvotorService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EvotorServiceRepository extends JpaRepository<EvotorService, Integer> {

    EvotorService findByEvotorServiceUUID(String evotorServiceUUID);

    EvotorService findBydSystemALTCode(String dSystemALTCode);

    List<EvotorService> findAll();

    <S extends EvotorService> S save(S token);
}

