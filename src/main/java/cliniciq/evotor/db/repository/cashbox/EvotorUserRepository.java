package cliniciq.evotor.db.repository.cashbox;

import cliniciq.evotor.db.domain.cashbox.EvotorUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EvotorUserRepository extends JpaRepository<EvotorUser, Integer> {

    EvotorUser findByEvotorUserUUID(String evotorUserUUID);

    EvotorUser findByDSystemUserId(Integer dSystemUserId);

    EvotorUser findByEvotorUserUUIDAndEvotorShops_EvotorUUID(String evotorUserUUID, String evotorUUID);

    List<EvotorUser> findAll();

    <S extends EvotorUser> S save(S token);
}

