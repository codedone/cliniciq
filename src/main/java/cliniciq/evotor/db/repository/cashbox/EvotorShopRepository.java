package cliniciq.evotor.db.repository.cashbox;

import cliniciq.evotor.db.domain.cashbox.EvotorShop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EvotorShopRepository extends JpaRepository<EvotorShop, Integer> {

    EvotorShop findByEvotorUUID(String evotorUUID);

    EvotorShop findByDSystemDomainId(Integer dSystemDomainId);

    List<EvotorShop> findAll();

    <S extends EvotorShop> S save(S shop);
}

