package cliniciq.evotor.db.repository.cashbox;

import cliniciq.evotor.db.domain.cashbox.ClinicIQCheck;
import cliniciq.evotor.db.domain.cashbox.EvotorPosUser;
import cliniciq.evotor.db.domain.enums.CheckStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClinicIQCheckRepository extends JpaRepository<ClinicIQCheck, Integer> {
    List<ClinicIQCheck> findByCheckStatus(CheckStatus status);

    <S extends EvotorPosUser> S save(S token);
}

