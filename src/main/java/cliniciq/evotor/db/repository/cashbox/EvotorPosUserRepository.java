package cliniciq.evotor.db.repository.cashbox;

import cliniciq.evotor.db.domain.cashbox.EvotorPosUser;
import cliniciq.evotor.db.domain.cashbox.EvotorShop;
import cliniciq.evotor.db.domain.cashbox.EvotorUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EvotorPosUserRepository extends JpaRepository<EvotorPosUser, Integer> {

    EvotorPosUser findByEvotorDeviceId(String evotorDeviceId);

    EvotorPosUser findByEvotorShopAndEvotorUser(EvotorShop evotorShop, EvotorUser evotorUser);

    List<EvotorPosUser> findByEvotorShop(EvotorShop evotorShop);

    List<EvotorPosUser> findAll();

    <S extends EvotorPosUser> S save(S token);
}

