package cliniciq.evotor.db.domain.enums;

public enum CheckStatus {
    RECEIVED, PROCESSED, SEND, ERROR
}
