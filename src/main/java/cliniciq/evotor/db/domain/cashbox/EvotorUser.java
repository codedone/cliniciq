package cliniciq.evotor.db.domain.cashbox;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "EVOTOR_USER", indexes = {@Index(name = "IDX_USER_IDX1", columnList = "EVOTOR_USER_UUID")})
public class EvotorUser {

    @Id
    @SequenceGenerator(name = "SEQ_USR", sequenceName = "SEQ_EVOTOR_USER", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_EVOTOR_USER")
    private Integer id;

    @NotNull
    @Length(max = 100)
    @Column(name = "EVOTOR_USER_UUID", unique = true, nullable = false)
    private String evotorUserUUID;

    @Column(name = "DSYSTEM_USER_ID")
    private Integer dSystemUserId;

    @Length(max = 500)
    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "evotorUsers")
    private Set<EvotorShop> evotorShops = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "evotorUser")
    private Set<EvotorPosUser> evotorPosUsers = new HashSet<>();

    @PreRemove
    private void cleanShopsRelations() {
        this.getEvotorShops().forEach(shop -> shop.getEvotorUsers().remove(this));
        this.getEvotorShops().clear();
    }

    public Integer getId() {
        return id;
    }

    public EvotorUser setId(Integer id) {
        this.id = id;
        return this;
    }

    public Set<EvotorShop> getEvotorShops() {
        return evotorShops;
    }

    public EvotorUser setEvotorShops(Set<EvotorShop> evotorShops) {
        this.evotorShops = evotorShops;
        return this;
    }

    public Integer getDSystemUserId() {
        return dSystemUserId;
    }

    public EvotorUser setDSystemUserId(Integer dSystemUserId) {
        this.dSystemUserId = dSystemUserId;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public EvotorUser setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getEvotorUserUUID() {
        return evotorUserUUID;
    }

    public EvotorUser setEvotorUserUUID(String evotorUserUUID) {
        this.evotorUserUUID = evotorUserUUID;
        return this;
    }

    public Set<EvotorPosUser> getEvotorPosUsers() {
        return evotorPosUsers;
    }

    public void setEvotorPosUsers(Set<EvotorPosUser> evotorPosUsers) {
        this.evotorPosUsers = evotorPosUsers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EvotorUser that = (EvotorUser) o;
        return Objects.equals(evotorUserUUID, that.evotorUserUUID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(evotorUserUUID);
    }
}
