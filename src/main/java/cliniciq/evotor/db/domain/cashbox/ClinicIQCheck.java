package cliniciq.evotor.db.domain.cashbox;

import cliniciq.evotor.db.domain.enums.CheckStatus;

import javax.persistence.*;
import java.util.Objects;

//TODO CHECK_JSON type change to PostgreSQL JSON

@Entity
@Table(name = "CLINICIQ_CHECK")
public class ClinicIQCheck {

    @Id
    @SequenceGenerator(name = "SEQ_CC", sequenceName = "SEQ_CLINICIQ_CHECK", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_CLINICIQ_CHECK")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "CHECK_STATUS")
    private CheckStatus checkStatus;

    @Lob
    @Column(name = "CHECK_JSON")
    private String checkJson;

    public Long getId() {
        return id;
    }

    public ClinicIQCheck setId(Long id) {
        this.id = id;
        return this;
    }

    public CheckStatus getCheckStatus() {
        return checkStatus;
    }

    public ClinicIQCheck setCheckStatus(CheckStatus checkStatus) {
        this.checkStatus = checkStatus;
        return this;
    }

    public String getCheckJson() {
        return checkJson;
    }

    public ClinicIQCheck setCheckJson(String checkJson) {
        this.checkJson = checkJson;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClinicIQCheck that = (ClinicIQCheck) o;
        return Objects.equals(checkJson, that.checkJson);
    }

    @Override
    public int hashCode() {
        return Objects.hash(checkJson);
    }
}