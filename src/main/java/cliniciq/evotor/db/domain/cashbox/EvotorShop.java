package cliniciq.evotor.db.domain.cashbox;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "EVOTOR_SHOP", indexes = {@Index(name = "IDX_SHOP_IDX1", columnList = "EVOTOR_UUID")})
public class EvotorShop {

    @Id
    @SequenceGenerator(name = "SEQ_ESH", sequenceName = "SEQ_EVOTOR_SHOP", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_EVOTOR_SHOP")
    private Integer id;

    @NotNull
    @Length(max = 100)
    @Column(name = "EVOTOR_UUID", unique = true, nullable = false)
    private String evotorUUID;

    @Column(name = "DSYSTEM_DOMAIN_ID")
    private Integer dSystemDomainId;

    @Length(max = 500)
    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinTable(name = "shop_users",
            joinColumns = @JoinColumn(name = "evotor_shop_id"),
            inverseJoinColumns = @JoinColumn(name = "evotor_user_id"))
    private Set<EvotorUser> evotorUsers = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "evotorShop")
    private Set<EvotorService> evotorServices = new HashSet<>();

    @PreRemove
    private void cleanUserRelations() {
        this.getEvotorUsers().forEach(user -> user.getEvotorShops().remove(this));
        this.getEvotorUsers().clear();
    }

    public Integer getId() {
        return id;
    }

    public EvotorShop setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getEvotorUUID() {
        return evotorUUID;
    }

    public EvotorShop setEvotorUUID(String evotorUUID) {
        this.evotorUUID = evotorUUID;
        return this;
    }

    public Integer getDSystemDomainId() {
        return dSystemDomainId;
    }

    public EvotorShop setDSystemDomainId(Integer dSystemDomainId) {
        this.dSystemDomainId = dSystemDomainId;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public EvotorShop setDescription(String description) {
        this.description = description;
        return this;
    }

    public Set<EvotorUser> getEvotorUsers() {
        return evotorUsers;
    }

    public EvotorShop setEvotorUsers(Set<EvotorUser> evotorUsers) {
        this.evotorUsers = evotorUsers;
        return this;
    }

    public Set<EvotorService> getEvotorServices() {
        return evotorServices;
    }

    public void setEvotorServices(Set<EvotorService> evotorServices) {
        this.evotorServices = evotorServices;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EvotorShop that = (EvotorShop) o;
        return Objects.equals(evotorUUID, that.evotorUUID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(evotorUUID);
    }

    @Override
    public String toString() {
        return "EvotorShop{" +
                "id=" + id +
                ", evotorUUID='" + evotorUUID + '\'' +
                ", dSystemDomainId=" + dSystemDomainId +
                ", description='" + description + '\'' +
                ", evotorUsers=" + evotorUsers +
                ", evotorServices=" + evotorServices +
                '}';
    }
}
