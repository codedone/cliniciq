package cliniciq.evotor.db.domain.cashbox;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "EVOTOR_POS_USER", indexes = {@Index(name = "IDX_POS_IDX1", columnList = "EVOTOR_DEVICE_ID")})
public class EvotorPosUser {

    @Id
    @SequenceGenerator(name = "SEQ_EPU", sequenceName = "SEQ_EVOTOR_POS_USER", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_EVOTOR_POS_USER")
    private Integer id;

    @Length(max = 100)
    @Column(name = "EVOTOR_DEVICE_ID")
    private String evotorDeviceId;

    @Length(max = 500)
    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EVOTOR_SHOP_ID", nullable = false)
    private EvotorShop evotorShop;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EVOTOR_USER_ID")
    private EvotorUser evotorUser;

    public Integer getId() {
        return id;
    }

    public EvotorPosUser setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getEvotorDeviceId() {
        return evotorDeviceId;
    }

    public EvotorPosUser setEvotorDeviceId(String evotorDeviceId) {
        this.evotorDeviceId = evotorDeviceId;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public EvotorPosUser setDescription(String description) {
        this.description = description;
        return this;
    }

    public EvotorShop getEvotorShop() {
        return evotorShop;
    }

    public EvotorPosUser setEvotorShop(EvotorShop evotorShop) {
        this.evotorShop = evotorShop;
        return this;
    }

    public EvotorUser getEvotorUser() {
        return evotorUser;
    }

    public EvotorPosUser setEvotorUser(EvotorUser evotorUser) {
        this.evotorUser = evotorUser;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EvotorPosUser that = (EvotorPosUser) o;
        return Objects.equals(evotorDeviceId, that.evotorDeviceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(evotorDeviceId);
    }
}
