package cliniciq.evotor.db.domain.cashbox;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "EVOTOR_SERVICE", indexes = {@Index(name = "IDX_SERVICE_IDX1", columnList = "EVOTOR_SERVICE_UUID")})
public class EvotorService {

    @Id
    @SequenceGenerator(name = "SEQ_ESE", sequenceName = "SEQ_EVOTOR_SERVICE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_EVOTOR_SERVICE")
    private Integer id;

    @Length(max = 100)
    @Column(name = "EVOTOR_SERVICE_UUID", unique = true, nullable = false)
    private String evotorServiceUUID;

    @Length(max = 100)
    @Column(name = "DSYSTEM_ALT_CODE")
    private String dSystemALTCode;

    @Column(name = "TYPE", nullable = false)
    private String type;

    @Column(name = "MEASURE_NAME", nullable = false)
    private String measureName;

    @Length(max = 500)
    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EVOTOR_SHOP_ID", nullable = false)
    private EvotorShop evotorShop;

    public Integer getId() {
        return id;
    }

    public EvotorService setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getEvotorServiceUUID() {
        return evotorServiceUUID;
    }

    public EvotorService setEvotorServiceUUID(String evotorServiceUUID) {
        this.evotorServiceUUID = evotorServiceUUID;
        return this;
    }

    public String getDSystemALTCode() {
        return dSystemALTCode;
    }

    public EvotorService setDSystemALTCode(String dSystemALTCode) {
        this.dSystemALTCode = dSystemALTCode;
        return this;
    }

    public String getType() {
        return type;
    }

    public EvotorService setType(String type) {
        this.type = type;
        return this;
    }

    public String getMeasureName() {
        return measureName;
    }

    public EvotorService setMeasureName(String measureName) {
        this.measureName = measureName;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public EvotorService setDescription(String description) {
        this.description = description;
        return this;
    }

    public EvotorShop getEvotorShop() {
        return evotorShop;
    }

    public EvotorService setEvotorShop(EvotorShop evotorShop) {
        this.evotorShop = evotorShop;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EvotorService that = (EvotorService) o;
        return Objects.equals(evotorServiceUUID, that.evotorServiceUUID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(evotorServiceUUID);
    }
}
