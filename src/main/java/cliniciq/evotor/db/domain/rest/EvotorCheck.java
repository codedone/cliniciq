package cliniciq.evotor.db.domain.rest;

import cliniciq.evotor.db.domain.enums.CheckStatus;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "EVOTOR_CHECK")
public class EvotorCheck {

    @Id
    @SequenceGenerator(name = "SEQ_ECH", sequenceName = "SEQ_EVOTOR_CHECK", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_EVOTOR_CHECK")
    private Long id;

    @Column(name = "SHOP_UUID")
    private String shopUUID;

    @Column(name = "USER_UUID")
    private String userUUID;

    @Column(name = "DEVICE_UUID")
    private String deviceUUID;

    @Enumerated(EnumType.STRING)
    @Column(name = "CHECK_STATUS")
    private CheckStatus checkStatus;

    @Lob
    @Column(name = "CHECK_JSON")
    private String checkJson;

    public Long getId() {
        return id;
    }

    public EvotorCheck setId(Long id) {
        this.id = id;
        return this;
    }

    public String getShopUUID() {
        return shopUUID;
    }

    public EvotorCheck setShopUUID(String shopUUID) {
        this.shopUUID = shopUUID;
        return this;
    }

    public String getUserUUID() {
        return userUUID;
    }

    public EvotorCheck setUserUUID(String userUUID) {
        this.userUUID = userUUID;
        return this;
    }

    public String getDeviceUUID() {
        return deviceUUID;
    }

    public EvotorCheck setDeviceUUID(String deviceUUID) {
        this.deviceUUID = deviceUUID;
        return this;
    }

    public CheckStatus getCheckStatus() {
        return checkStatus;
    }

    public EvotorCheck setCheckStatus(CheckStatus checkStatus) {
        this.checkStatus = checkStatus;
        return this;
    }

    public String getCheckJson() {
        return checkJson;
    }

    public EvotorCheck setCheckJson(String checkJson) {
        this.checkJson = checkJson;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EvotorCheck that = (EvotorCheck) o;
        return Objects.equals(shopUUID, that.shopUUID) &&
                Objects.equals(userUUID, that.userUUID) &&
                Objects.equals(deviceUUID, that.deviceUUID) &&
                Objects.equals(checkJson, that.checkJson);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shopUUID, userUUID, deviceUUID, checkJson);
    }
}