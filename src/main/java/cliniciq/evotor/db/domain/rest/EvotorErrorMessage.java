package cliniciq.evotor.db.domain.rest;

public class EvotorErrorMessage {
    private EvotorErrorCode code;
    private String reason;
    private String subject;

    public static EvotorErrorMessage of(EvotorErrorCode code) {
        return new EvotorErrorMessage().setCode(code);
    }

    public EvotorErrorCode getCode() {
        return code;
    }

    private EvotorErrorMessage setCode(EvotorErrorCode code) {
        this.code = code;
        return this;
    }

    public String getReason() {
        return reason;
    }

    private EvotorErrorMessage setReason(String reason) {
        this.reason = reason;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    private EvotorErrorMessage setSubject(String subject) {
        this.subject = subject;
        return this;
    }
}
