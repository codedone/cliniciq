package cliniciq.evotor.db.domain.rest;

import com.fasterxml.jackson.annotation.JsonValue;

public enum EvotorErrorCode {
    INCORRECT_EVOTOR_TOKEN(1001),
    INCORRECT_EVOTOR_USER_TOKEN(1002),
    EVOTOR_USER_TOKEN_NO_LONGER_VALID(1003),
    INVALID_USER_CREDENTIALS(1006),
    SYNTACTIC_ERROR(2001),
    REQUIRED_FIELD_NOT_PRESENT(2002),
    ASSOCIATION_ERROR(2004);

    private final Integer code;

    EvotorErrorCode(Integer code) {
        this.code = code;
    }

    @JsonValue
    public Integer getCode() {
        return code;
    }
}
