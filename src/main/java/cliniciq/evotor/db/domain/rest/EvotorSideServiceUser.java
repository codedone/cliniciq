package cliniciq.evotor.db.domain.rest;

import cliniciq.evotor.utils.converter.DateTimeConverter;
import cliniciq.evotor.utils.converter.EvotorSideServiceUserDeserializer;
import cliniciq.evotor.utils.converter.EvotorSideServiceUserSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Objects;
import org.joda.time.LocalDateTime;

import javax.persistence.*;

@JsonSerialize(using = EvotorSideServiceUserSerializer.class)
@JsonDeserialize(using = EvotorSideServiceUserDeserializer.class)
@Entity
@Table(name = "EVOTOR_SIDE_SERVICE_USER")
public class EvotorSideServiceUser {

    @Id
    @SequenceGenerator(name = "SEQ_ESSU", sequenceName = "SEQ_EVOTOR_SIDE_SERVICE_USER", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_EVOTOR_SIDE_SERVICE_USER")
    private Integer id;

    @Column(name = "CREATION_DATE_TIME")
    @Convert(converter = DateTimeConverter.class)
    private LocalDateTime creationDateTime = LocalDateTime.now();

    @Column(name = "USER_ID")
    private String userId;

    @Column(name = "SHOP_ID")
    private String shopId;

    @Column(name = "TOKEN", unique = true)
    private String token;

    @Column(name = "HAS_BILLING")
    private boolean hasBilling = true;

    @Column(name = "USER_NAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    public Integer getId() {
        return id;
    }

    public EvotorSideServiceUser setId(Integer id) {
        this.id = id;
        return this;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public EvotorSideServiceUser setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public EvotorSideServiceUser setUsername(String userName) {
        this.username = userName;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public EvotorSideServiceUser setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public EvotorSideServiceUser setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public boolean isHasBilling() {
        return hasBilling;
    }

    public EvotorSideServiceUser setHasBilling(boolean hasBilling) {
        this.hasBilling = hasBilling;
        return this;
    }

    public String getShopId() {
        return shopId;
    }

    public EvotorSideServiceUser setShopId(String shopId) {
        this.shopId = shopId;
        return this;
    }

    public String getToken() {
        return token;
    }

    public EvotorSideServiceUser setToken(String token) {
        this.token = token;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EvotorSideServiceUser that = (EvotorSideServiceUser) o;
        return Objects.equal(userId, that.userId) &&
                Objects.equal(username, that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(userId, username);
    }
}
