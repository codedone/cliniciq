package cliniciq.evotor.db.domain.rest;

import cliniciq.evotor.utils.converter.DateTimeConverter;
import net.minidev.json.annotate.JsonIgnore;
import org.joda.time.LocalDateTime;

import javax.persistence.*;

@Entity
@Table(name = "EVOTOR_TOKEN")
public class EvotorToken {

    @Id
    @SequenceGenerator(name = "SEQ_ET", sequenceName = "SEQ_EVOTOR_TOKEN", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_EVOTOR_TOKEN")
    @JsonIgnore
    private Integer id;

    @Column(name = "CREATION_DATE_TIME")
    @Convert(converter = DateTimeConverter.class)
    @JsonIgnore
    private LocalDateTime creationDateTime = LocalDateTime.now();

    @Column(name = "TOKEN")
    private String token;

    public Integer getId() {
        return id;
    }

    public EvotorToken setId(Integer id) {
        this.id = id;
        return this;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public EvotorToken setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
        return this;
    }

    public String getToken() {
        return token;
    }

    public EvotorToken setToken(String token) {
        this.token = token;
        return this;
    }
}
