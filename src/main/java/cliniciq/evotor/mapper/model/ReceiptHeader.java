package cliniciq.evotor.mapper.model;

public class ReceiptHeader {

	private String clientPhone;
	private String clientEmail;

    public ReceiptHeader() {
    }

	public ReceiptHeader(String clientPhone, String clientEmail) {
		super();
		this.clientPhone = clientPhone;
		this.clientEmail = clientEmail;
	}
	
	
	public String getClientPhone() {
		return clientPhone;
	}
	public void setClientPhone(String clientPhone) {
		this.clientPhone = clientPhone;
	}
	public String getClientEmail() {
		return clientEmail;
	}
	public void setClientEmail(String clientEmail) {
		this.clientEmail = clientEmail;
	}
	
}
