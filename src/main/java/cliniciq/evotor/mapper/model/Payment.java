package cliniciq.evotor.mapper.model;

import java.math.BigDecimal;

public class Payment {
    /**
     * Uuid
     */
    private String uuid;
    /**
     * Сумма денег принятых от клиента
     */
    private BigDecimal value;
    /**
     * Платежная система
     */
    private PaymentSystem paymentSystem;

    public Payment() {
    }

	public Payment(String uuid, BigDecimal value, PaymentSystem paymentSystem) {
		super();
		this.uuid = uuid;
		this.value = value;
		this.paymentSystem = paymentSystem;
	}
	
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}


	public PaymentSystem getPaymentSystem() {
		return paymentSystem;
	}


	public void setPaymentSystem(PaymentSystem paymentSystem) {
		this.paymentSystem = paymentSystem;
	}

    
}
