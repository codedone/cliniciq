package cliniciq.evotor.mapper.model;

public class PaymentSystem {
	private String 	paymentSystemId; 
	private PaymentType paymentType; 
	private String userDescription;
	
	public PaymentSystem() {

	}
	
	public PaymentSystem(String paymentSystemId, PaymentType paymentType, String userDescription) {
		super();
		this.paymentSystemId = paymentSystemId;
		this.paymentType = paymentType;
		this.userDescription = userDescription;
	}
	
	public String getPaymentSystemId() {
		return paymentSystemId;
	}
	public void setPaymentSystemId(String paymentSystemId) {
		this.paymentSystemId = paymentSystemId;
	}
	public PaymentType getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}
	public String getUserDescription() {
		return userDescription;
	}
	public void setUserDescription(String userDescription) {
		this.userDescription = userDescription;
	}
}
