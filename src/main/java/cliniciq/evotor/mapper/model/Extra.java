package cliniciq.evotor.mapper.model;

public class Extra {
    private Integer domainId;
    private Integer userId;

    public Integer getDomainId() {
        return domainId;
    }

    public Extra setDomainId(Integer domainId) {
        this.domainId = domainId;
        return this;
    }

    public Integer getUserId() {
        return userId;
    }

    public Extra setUserId(Integer userId) {
        this.userId = userId;
        return this;
    }
}
