package cliniciq.evotor.mapper.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum ProductType {
    /**
     * Товар
     */
    NORMAL,

    /**
     * Маркированный алкоголь
     */
    ALCOHOL_MARKED,

    /**
     * Немаркированный алкоголь
     */
    ALCOHOL_NOT_MARKED,

    /**
     * Услуга
     */
    SERVICE,

    /**
     * Маркированный табак
     */
    TOBACCO_MARKED
}
