package cliniciq.evotor.mapper.model;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Purchaser {
	public Purchaser() {
	}
	//Наименование покупателя, например, название организации. Данные сохраняются в теге 1227 фискального документа.
	private String name;
	//Номер документа покупателя, например, ИНН или номер паспорта иностранного гражданина. Данные сохраняются в теге 1228 фискального документа.
	private String documentNumber;
	//Тип покупателя, например, физическое лицо. Не сохраняется в фискальном документе.
	private PurchaserType type;

    public Purchaser(String name, String documentNumber, PurchaserType type) {
		super();
		this.name = name;
		this.documentNumber = documentNumber;
		this.type = type;
	}

	//Тип покупателя. Не сохраняется в фискальном документе.
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	public static enum PurchaserType {
		//Индивидуальный предприниматель.
		ENTREPRENEUR,
		//Юридическое лицо.
		LEGAL_ENTITY,
		//Физическое лицо.
		NATURAL_PERSON
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public PurchaserType getType() {
		return type;
	}

	public void setType(PurchaserType type) {
		this.type = type;
	}

}
