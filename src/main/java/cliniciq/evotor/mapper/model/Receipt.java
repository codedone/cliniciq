package cliniciq.evotor.mapper.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;


public class Receipt {
	
	private ReceiptHeader header;
	
	private String uuid;
	private String receiptNumber;
	private ReceiptType type;
	
	private BigDecimal discount;
	private PrintGroup printGroup; //элемент кассового чека, содержащий данные об организации, которая осуществляет торговую операцию
	private Payment[] payments; //список всех оплат
	private Position[] positions; //список всех позиций чека

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String evotorDeviceId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Extra extra;

	public Receipt() {
	}

	public Receipt(ReceiptHeader header, String uuid, String receiptNumber, ReceiptType type, BigDecimal discount,
                   PrintGroup printGroup,
                   Payment[] payments,
                   Position[] positions,
                   String evotorDeviceId) {
		super();
		this.header = header;
		this.uuid = uuid;
		this.receiptNumber = receiptNumber;
		this.type = type;
		this.discount = discount;
		this.printGroup = printGroup;
		this.payments = payments;
		this.positions = positions;
        this.evotorDeviceId = evotorDeviceId;
	}
	
	public PrintGroup getPrintGroup() {
		return printGroup;
	}

	public void setPrintGroup(PrintGroup printGroup) {
		this.printGroup = printGroup;
	}

	public ReceiptHeader getHeader() {
		return header;
	}

	public void setHeader(ReceiptHeader header) {
		this.header = header;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public ReceiptType getType() {
		return type;
	}

	public void setType(ReceiptType type) {
		this.type = type;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public Payment[] getPayments() {
		return payments;
	}

	public void setPayments(Payment[] payments) {
		this.payments = payments;
	}

	public Position[] getPositions() {
		return positions;
	}

	public void setPositions(Position[] positions) {
		this.positions = positions;
	}

    public String getEvotorDeviceId() {
        return evotorDeviceId;
    }

    public void setEvotorDeviceId(String evotorDeviceId) {
        this.evotorDeviceId = evotorDeviceId;
	}

    public Extra getExtra() {
        return extra;
    }

    public Receipt setExtra(Extra extra) {
        this.extra = extra;
        return this;
    }
}
