package cliniciq.evotor.mapper.model;

import com.fasterxml.jackson.annotation.JsonFormat;

//Печатная группа – элемент кассового чека, содержащий данные об организации, которая осуществляет торговую операцию.
public class PrintGroup {

	private String identifier;
	private String orgAddress;
	private String orgInn;
	private String orgName;
	private Purchaser purchaser;
	private TaxationSystem taxationSystem;
	private Type type;

	public PrintGroup() {

	}

	//
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	public static enum Type {
		//Кассовый чек, напечатанный средствами ККМ
		CASH_RECEIPT,
		//квитанция
		INVOICE,
		//ЕНВД чек, напечатанный строками
		STRING_UTII
	}

	public PrintGroup(String identifier, String orgAddress, String orgInn, String orgName, Purchaser purchaser,
			TaxationSystem taxationSystem, Type type) {
		super();
		this.identifier = identifier;
		this.orgAddress = orgAddress;
		this.orgInn = orgInn;
		this.orgName = orgName;
		this.purchaser = purchaser;
		this.taxationSystem = taxationSystem;
		this.type = type;
	}

	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public String getOrgAddress() {
		return orgAddress;
	}
	public void setOrgAddress(String orgAddress) {
		this.orgAddress = orgAddress;
	}
	public String getOrgInn() {
		return orgInn;
	}
	public void setOrgInn(String orgInn) {
		this.orgInn = orgInn;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public Purchaser getPurchaser() {
		return purchaser;
	}
	public void setPurchaser(Purchaser purchaser) {
		this.purchaser = purchaser;
	}
	public TaxationSystem getTaxationSystem() {
		return taxationSystem;
	}
	public void setTaxationSystem(TaxationSystem taxationSystem) {
		this.taxationSystem = taxationSystem;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	} 

	
}
