package cliniciq.evotor.mapper.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum TaxationSystem {
	COMMON,
	PATENT,
	SIMPLIFIED_INCOME,
	SIMPLIFIELD_INCOME_OUTCOME,
	SINGLE_AGRICULTURE,
	SINGLE_IMPUTED_INCOME 
}
