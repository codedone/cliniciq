package cliniciq.evotor.mapper.model;

import org.springframework.lang.Nullable;

import java.math.BigDecimal;

public class Position {
    /**
     * UUID товара.
     */
    @Nullable
    private String productUuid;
    /**
     * Код товара.
     */
    @Nullable
    private String productCode;
    /**
     * Вид товара.
     */
    private ProductType productType;
    /**
     * Наименование.
     */
    private String name;
    /**
     * Наименование единицы измерения.
     */
    private String measureName;
    /**
     * НДС
     */
    @Nullable
    private TaxNumber taxNumber;
    /**
     * Цена без скидок.
     */
    private BigDecimal price;
    /**
     * Цена с учетом скидки на позицию.
     */
    private BigDecimal priceWithDiscountPosition;
    /**
     * Количество.
     */
    private Integer quantity;

    public Position() {
    }

	public Position(String productUuid, String productCode, ProductType productType, String name, String measureName,
                    TaxNumber taxNumber, BigDecimal price, BigDecimal priceWithDiscountPosition, Integer quantity) {
		super();
		this.productUuid = productUuid;
		this.productCode = productCode;
		this.productType = productType;
		this.name = name;
		this.measureName = measureName;
		this.taxNumber = taxNumber;
		this.price = price;
		this.priceWithDiscountPosition = priceWithDiscountPosition;
		this.quantity = quantity;
	}
	
	public String getProductUuid() {
		return productUuid;
	}
	public void setProductUuid(String productUuid) {
		this.productUuid = productUuid;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public ProductType getProductType() {
		return productType;
	}
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMeasureName() {
		return measureName;
	}
	public void setMeasureName(String measureName) {
		this.measureName = measureName;
	}
	public TaxNumber getTaxNumber() {
		return taxNumber;
	}
	public void setTaxNumber(TaxNumber taxNumber) {
		this.taxNumber = taxNumber;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getPriceWithDiscountPosition() {
		return priceWithDiscountPosition;
	}
	public void setPriceWithDiscountPosition(BigDecimal priceWithDiscountPosition) {
		this.priceWithDiscountPosition = priceWithDiscountPosition;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
    
}
