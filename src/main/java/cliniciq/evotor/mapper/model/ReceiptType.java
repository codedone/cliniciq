package cliniciq.evotor.mapper.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum ReceiptType {
    BUY,   //Покупка
    BUYBACK, //Возврат покупки
    PAYBACK,  //Возврат
    SELL  //Продажа
}
