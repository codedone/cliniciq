package cliniciq.evotor.mapper.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;

@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum TaxNumber {
    /**
     * Основная ставка 18%. С первого января 2019 года может указывать как на 18%, так и на 20% ставку.
     */
    VAT_18(BigDecimal.valueOf(18)),
    /**
     * Основная ставка 10%.
     */
    VAT_10(BigDecimal.valueOf(10)),
    /**
     * Без НДС.
     */
    NO_VAT(BigDecimal.ZERO),
    /**
     * Расчётная ставка 18%. С первого января 2019 года может указывать как на 18%, так и на 20% ставку.
     */
    VAT_18_118(BigDecimal.valueOf(18)),
    /**
     * Расчётная ставка 10%.
     */
    VAT_10_110(BigDecimal.valueOf(10)),
    /**
     * Основная ставка 0%
     */
    VAT_0(BigDecimal.ZERO);

    private BigDecimal taxValue;

    TaxNumber(BigDecimal taxValue) {
        this.taxValue = taxValue;
    }

    public BigDecimal getTaxValue() {
        return this.taxValue;
    }	
}
