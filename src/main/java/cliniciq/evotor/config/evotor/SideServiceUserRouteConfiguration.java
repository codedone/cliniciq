package cliniciq.evotor.config.evotor;

import org.springframework.context.annotation.Configuration;
//TODO move to class level variable

@Configuration
public class SideServiceUserRouteConfiguration {
    public String authPath() {
        return "seda:evotor_side_service_user_auth_processor";
    }

    public String creationPath() {
        return "seda:evotor_side_service_user_creation_processor";
    }
}
