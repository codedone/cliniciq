package cliniciq.evotor.config.evotor;

import org.springframework.context.annotation.Configuration;
//TODO move to class level variable

@Configuration
public class NotificationRouteConfiguration {

    public String servicesPath() {
        return "seda:send_evotor_goods_dsystem_to_kafka";
    }

    public String posPath() {
        return "seda:send_evotor_pos_dsystem_to_kafka";
    }

    public String userPath() {
        return "seda:send_evotor_user_dsystem_to_kafka";
    }

    public String shopPath() {
        return "seda:send_evotor_store_dsystem_to_kafka";
    }

    public String checkPath() {
        return "seda:send_evotor_prepared_check_dsystem_to_kafka";
    }
}
