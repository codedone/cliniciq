package cliniciq.evotor.config;

import cliniciq.evotor.utils.enums.KafkaRouteType;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("!no_kafka")
@Configuration
public class KafkaRouteConfiguration {

    private final static String CONSUMER_PARAMETERS = "&maxPollRecords=1" +
            "&autoOffsetReset=earliest" +
            "&autoCommitEnable=false";

    private final static String PRODUCER_PARAMETERS = "&retries=0";

    private String buildRoutePath(String path, KafkaRouteType kafkaRouteType) {
        String DEFAULT_PARAMETERS = "?serializerClass=org.apache.kafka.common.serialization.StringSerializer" +
                "&keySerializerClass=org.apache.kafka.common.serialization.StringSerializer";

        return path + DEFAULT_PARAMETERS + (kafkaRouteType == KafkaRouteType.PRODUCER ? PRODUCER_PARAMETERS : CONSUMER_PARAMETERS);
    }

    public String posTopic(KafkaRouteType kafkaRouteType) {
        return buildRoutePath("kafka:Evotor_POS_Dsystem", kafkaRouteType);
    }

    public String userTopic(KafkaRouteType kafkaRouteType) {
        return buildRoutePath("kafka:Evotor_User_Dsystem", kafkaRouteType);
    }

    public String shopTopic(KafkaRouteType kafkaRouteType) {
        return buildRoutePath("kafka:Evotor_Store_Dsystem", kafkaRouteType);
    }

    public String receiptsTopic(KafkaRouteType kafkaRouteType) {
        return buildRoutePath("kafka:Evotor_Prepared_Check_Dsystem", kafkaRouteType);
    }

    public String servicesTopic(KafkaRouteType kafkaRouteType) {
        return buildRoutePath("kafka:Evotor_Goods_Dsystem", kafkaRouteType);
    }

    public String checkTopic(KafkaRouteType kafkaRouteType) {
        return buildRoutePath("kafka:Evotor_Payment_Dsystem", kafkaRouteType);
    }
}
