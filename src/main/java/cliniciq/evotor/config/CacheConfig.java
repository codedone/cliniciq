package cliniciq.evotor.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

import static cliniciq.evotor.db.service.rest.EvotorSideServiceUserService.EVOTOR_SIDE_SERVICE_USER_TOKEN_CACHE_KEY;
import static cliniciq.evotor.db.service.rest.EvotorTokenService.EVOTOR_TOKEN_CACHE_KEY;

@Configuration
@EnableCaching
public class CacheConfig {

    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        cacheManager.setCaches(Arrays.asList(
                new ConcurrentMapCache(EVOTOR_TOKEN_CACHE_KEY),
                new ConcurrentMapCache(EVOTOR_SIDE_SERVICE_USER_TOKEN_CACHE_KEY))
        );
        return cacheManager;
    }
}