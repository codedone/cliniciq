package cliniciq.evotor.utils.enums;

public class HTTPHeaderDictionary {
    public static final String X_EVOTOR_STORE_UUID = "X-Evotor-Store-Uuid";
    public static final String AUTHORIZATION = "Authorization";
    public static final String X_EVOTOR_DEVICE_UUID = "X-Evotor-Device-UUID";
    public static final String X_EVOTOR_USER_ID = "X-Evotor-User-Id";
}
