package cliniciq.evotor.utils.enums;

public enum KafkaRouteType {
    PRODUCER, CONSUMER
}