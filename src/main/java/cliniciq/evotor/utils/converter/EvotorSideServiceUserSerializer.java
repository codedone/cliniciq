package cliniciq.evotor.utils.converter;

import cliniciq.evotor.db.domain.rest.EvotorSideServiceUser;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class EvotorSideServiceUserSerializer extends JsonSerializer<EvotorSideServiceUser> {

    @Override
    public void serialize(EvotorSideServiceUser evotorSideServiceUser, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("userId", evotorSideServiceUser.getUserId());
        jsonGenerator.writeStringField("token", evotorSideServiceUser.getToken());
        jsonGenerator.writeBooleanField("hasBilling", evotorSideServiceUser.isHasBilling());
        jsonGenerator.writeEndObject();
    }
}