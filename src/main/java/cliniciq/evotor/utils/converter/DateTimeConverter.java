package cliniciq.evotor.utils.converter;

import org.joda.time.LocalDateTime;

import javax.persistence.AttributeConverter;
import java.sql.Date;

public class DateTimeConverter implements AttributeConverter<LocalDateTime, Date> {

    @Override
    public Date convertToDatabaseColumn(LocalDateTime localDateTime) {
        return localDateTime != null ? new Date(localDateTime.toDateTime().getMillis()) : null;
    }

    @Override
    public LocalDateTime convertToEntityAttribute(Date date) {
        return date != null ? new LocalDateTime(date) : null;
    }
}