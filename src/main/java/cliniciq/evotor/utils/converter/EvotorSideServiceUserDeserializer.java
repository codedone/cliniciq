package cliniciq.evotor.utils.converter;

import cliniciq.evotor.db.domain.rest.EvotorSideServiceUser;
import cliniciq.evotor.utils.exceptions.evotor.EvotorSideServiceUserParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class EvotorSideServiceUserDeserializer extends JsonDeserializer<EvotorSideServiceUser> {

    @Override
    public EvotorSideServiceUser deserialize(JsonParser jp, DeserializationContext ctxt) {
        try {
            JsonNode node = jp.getCodec().readTree(jp);
            String userId = node.get("userId").textValue();
            return new EvotorSideServiceUser().setUserId(userId);
        } catch (Exception e){
            throw new EvotorSideServiceUserParseException("Cannot parse fields to object");
        }
    }
}