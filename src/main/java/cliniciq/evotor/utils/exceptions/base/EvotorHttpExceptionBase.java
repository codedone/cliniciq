package cliniciq.evotor.utils.exceptions.base;

import cliniciq.evotor.db.domain.rest.EvotorErrorCode;

public abstract class EvotorHttpExceptionBase extends RuntimeException {
    private final int errorCode;
    private final EvotorErrorCode evotorErrorCode;

    public EvotorHttpExceptionBase(String message, int errorCode, EvotorErrorCode evotorErrorCode) {
        super(message);
        this.errorCode = errorCode;
        this.evotorErrorCode = evotorErrorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public EvotorErrorCode getEvotorErrorCode() {
        return evotorErrorCode;
    }
}
