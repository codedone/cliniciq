package cliniciq.evotor.utils.exceptions.mapping;

public class CannotFindEvotorPosUserMappingException extends CheckMappingException {
    public CannotFindEvotorPosUserMappingException(String message) {
        super(message);
    }
}
