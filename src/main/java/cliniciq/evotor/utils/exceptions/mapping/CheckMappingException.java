package cliniciq.evotor.utils.exceptions.mapping;

public class CheckMappingException extends RuntimeException {
    public CheckMappingException(String message) {
        super(message);
    }
}
