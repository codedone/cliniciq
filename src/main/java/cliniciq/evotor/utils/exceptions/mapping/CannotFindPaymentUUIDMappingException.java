package cliniciq.evotor.utils.exceptions.mapping;

public class CannotFindPaymentUUIDMappingException extends CheckMappingException {
    public CannotFindPaymentUUIDMappingException(String message) {
        super(message);
    }
}
