package cliniciq.evotor.utils.exceptions.db;

public class ShopNotFoundDBException extends RuntimeException {
    public ShopNotFoundDBException(String message) {
        super(message);
    }
}
