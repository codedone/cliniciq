package cliniciq.evotor.utils.exceptions.db;

public class CheckRequestSaveDBException extends RuntimeException {
    public CheckRequestSaveDBException(String message) {
        super(message);
    }
}
