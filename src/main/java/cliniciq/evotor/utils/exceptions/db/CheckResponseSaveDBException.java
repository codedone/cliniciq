package cliniciq.evotor.utils.exceptions.db;

public class CheckResponseSaveDBException extends RuntimeException {
    public CheckResponseSaveDBException(String message) {
        super(message);
    }
}
