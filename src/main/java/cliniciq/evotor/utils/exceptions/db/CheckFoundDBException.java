package cliniciq.evotor.utils.exceptions.db;

public class CheckFoundDBException extends RuntimeException {
    public CheckFoundDBException(String message) {
        super(message);
    }
}
