package cliniciq.evotor.utils.exceptions.evotor;

import cliniciq.evotor.db.domain.rest.EvotorErrorCode;
import cliniciq.evotor.utils.exceptions.base.EvotorHttpExceptionBase;

public class EvotorAuthenticationException  extends EvotorHttpExceptionBase {
    public EvotorAuthenticationException(String message) {
        super(message, 401, EvotorErrorCode.INCORRECT_EVOTOR_TOKEN);
    }
}
