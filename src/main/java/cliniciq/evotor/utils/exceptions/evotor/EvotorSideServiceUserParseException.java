package cliniciq.evotor.utils.exceptions.evotor;

import cliniciq.evotor.db.domain.rest.EvotorErrorCode;
import cliniciq.evotor.utils.exceptions.base.EvotorHttpExceptionBase;

public class EvotorSideServiceUserParseException extends EvotorHttpExceptionBase {
    public EvotorSideServiceUserParseException(String message) {
        super(message, 400, EvotorErrorCode.REQUIRED_FIELD_NOT_PRESENT);
    }
}
