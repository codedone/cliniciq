package cliniciq.evotor.utils.exceptions.evotor;

import cliniciq.evotor.db.domain.rest.EvotorErrorCode;
import cliniciq.evotor.utils.exceptions.base.EvotorHttpExceptionBase;

public class EvotorSideServiceUserAssociationException extends EvotorHttpExceptionBase {
    public EvotorSideServiceUserAssociationException(String message) {
        super(message, 409, EvotorErrorCode.ASSOCIATION_ERROR);
    }
}
