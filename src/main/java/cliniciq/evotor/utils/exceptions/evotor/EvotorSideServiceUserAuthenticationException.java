package cliniciq.evotor.utils.exceptions.evotor;

import cliniciq.evotor.db.domain.rest.EvotorErrorCode;
import cliniciq.evotor.utils.exceptions.base.EvotorHttpExceptionBase;

public class EvotorSideServiceUserAuthenticationException extends EvotorHttpExceptionBase {
    public EvotorSideServiceUserAuthenticationException(String message) {
        super(message, 401, EvotorErrorCode.INVALID_USER_CREDENTIALS);
    }
}
