package cliniciq.evotor.utils.camel;

import org.apache.camel.Exchange;
import org.apache.camel.component.kafka.KafkaConstants;
import org.apache.camel.component.kafka.KafkaManualCommit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class KafkaCamelUtils {
    private final static Logger LOGGER = LoggerFactory.getLogger(KafkaCamelUtils.class);

    public static void manualCommit(Exchange exchange) {
        Boolean lastOne = exchange.getIn().getHeader(KafkaConstants.LAST_RECORD_BEFORE_COMMIT, Boolean.class);

        Throwable exception = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Throwable.class);
        if (Objects.nonNull(exception)) {
            LOGGER.info("cannot commit offset, exception occurred in route");
            return;
        }

        if (lastOne) {
            KafkaManualCommit manual = exchange.getIn().getHeader(KafkaConstants.MANUAL_COMMIT, KafkaManualCommit.class);
            if (manual != null) {
                LOGGER.info("manually committing the offset for batch");
                manual.commitSync();
            }
        } else {
            LOGGER.info("NOT time to commit the offset yet");
        }
    }
}
