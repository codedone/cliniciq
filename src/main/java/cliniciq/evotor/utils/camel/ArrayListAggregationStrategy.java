package cliniciq.evotor.utils.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import java.util.ArrayList;
import java.util.List;

//TODO code analyzer dont understand that type was checked in if statement
@SuppressWarnings("unchecked")
public class ArrayListAggregationStrategy implements AggregationStrategy {

    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        Message newIn = newExchange.getIn();
        Object newBody = newIn.getBody();
        if (oldExchange == null) {
            List<Object> list = new ArrayList<>();
            list.add(newBody);
            newIn.setBody(list);
            return newExchange;
        } else {
            Message in = oldExchange.getIn();
            if (in.getBody() instanceof ArrayList) {
                ArrayList list = in.getBody(ArrayList.class);
                list.add(newBody);
            } else {
                oldExchange.getIn().setBody(new ArrayList<>());
            }
            return oldExchange;
        }
    }

}