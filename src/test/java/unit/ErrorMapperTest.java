package unit;

import cliniciq.evotor.db.domain.cashbox.*;
import cliniciq.evotor.db.domain.enums.CheckStatus;
import cliniciq.evotor.db.domain.rest.EvotorCheck;
import cliniciq.evotor.db.service.cashbox.*;
import cliniciq.evotor.db.service.rest.EvotorCheckService;
import cliniciq.evotor.routes.cliniciq.CheckErrorRetryRoute;
import cliniciq.evotor.service.CheckMappingService;
import org.apache.camel.CamelContext;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import utils.CheckServiceMock;
import utils.EvotorCheckServiceMock;
import utils.EvotorServiceMock;
import utils.ResourceTest;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.google.common.collect.Lists.newArrayList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeast;

@ActiveProfiles("test")
@EnableAutoConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {CheckMappingService.class, EvotorServiceMock.class, CheckServiceMock.class, EvotorCheckServiceMock.class, CheckErrorRetryRoute.class})
public class ErrorMapperTest extends ResourceTest {

    @Autowired
    private CamelContext camelContext;

    @Autowired
    private CheckErrorRetryRoute checkErrorRetryRoute;

    @Autowired
    private EvotorShopService evotorShopService;

    @Autowired
    private EvotorUserService evotorUserService;

    @Autowired
    private EvotorServiceService evotorServiceService;

    @Autowired
    private EvotorPosUserService evotorPosUserService;

    @Autowired
    private ClinicIQCheckService clinicIQCheckService;

    @MockBean
    private EvotorCheckService evotorCheckServiceMock;

    @BeforeClass
    public static void beforeTestClass() {
        System.setProperty("cron.remapping", "0/10+*+*+*+*+?");
    }

    @Before
    public void beforeTest() throws Exception {
        camelContext.addRoutes(checkErrorRetryRoute);
    }

    @Test
    public void testErrorRetryService() throws Exception {
        Integer domainId = 12345;
        Integer userId = 98765;

        String shopUUID = UUID.randomUUID().toString();
        String userUUID = UUID.randomUUID().toString();
        String deviceUUID = UUID.randomUUID().toString();

        String json = readPaymentJsonFromResources(domainId, userId);
        ClinicIQCheck evotorCheck = new ClinicIQCheck().setCheckJson(json).setCheckStatus(CheckStatus.ERROR);
        Mockito.when(clinicIQCheckService.findAllErrors()).thenReturn(newArrayList(evotorCheck));

        Mockito.when(evotorCheckServiceMock.save(any(EvotorCheck.class))).thenReturn(new EvotorCheck());

        EvotorShop evotorShop = new EvotorShop().setId(domainId).setEvotorUUID(shopUUID);
        Mockito.when(evotorShopService.findByDSystemDomainId(domainId)).thenReturn(evotorShop);

        EvotorUser evotorUser = new EvotorUser().setId(userId).setEvotorUserUUID(userUUID);
        Mockito.when(evotorUserService.findByDSystemUserId(userId)).thenReturn(evotorUser);

        EvotorPosUser evotorPosUser = new EvotorPosUser().setEvotorDeviceId(deviceUUID).setEvotorShop(evotorShop).setEvotorUser(evotorUser);
        Mockito.when(evotorPosUserService.findByEvotorUserAndEvotorShop(evotorShop, evotorUser)).thenReturn(evotorPosUser);
        Mockito.when(evotorServiceService.findByDSystemALTCode(any(String.class))).thenReturn(new EvotorService().setEvotorServiceUUID(UUID.randomUUID().toString()));

        TimeUnit.SECONDS.sleep(15);
        Mockito.verify(evotorCheckServiceMock, atLeast(1)).save(any(EvotorCheck.class));
    }
}
