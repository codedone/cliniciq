package unit;

import cliniciq.evotor.db.domain.cashbox.*;
import cliniciq.evotor.db.domain.rest.EvotorCheck;
import cliniciq.evotor.db.service.cashbox.EvotorPosUserService;
import cliniciq.evotor.db.service.cashbox.EvotorServiceService;
import cliniciq.evotor.db.service.cashbox.EvotorShopService;
import cliniciq.evotor.db.service.cashbox.EvotorUserService;
import cliniciq.evotor.service.CheckMappingService;
import cliniciq.evotor.utils.exceptions.mapping.CannotFindEvotorPosUserMappingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import utils.EvotorServiceMock;
import utils.ResourceTest;

import java.io.IOException;
import java.util.UUID;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {EvotorServiceMock.class, CheckMappingService.class})
public class MapperTest extends ResourceTest {

    @Autowired
    private CheckMappingService checkMappingService;

    @Autowired
    private EvotorShopService evotorShopService;

    @Autowired
    private EvotorUserService evotorUserService;

    @Autowired
    private EvotorServiceService evotorServiceService;

    @Autowired
    private EvotorPosUserService evotorPosUserService;


    @Test
    public void testMapReceiptWithShopAndUser() throws IOException {
        Integer domainId = 12345;
        Integer userId = 98765;

        String shopUUID = UUID.randomUUID().toString();
        String userUUID = UUID.randomUUID().toString();
        String deviceUUID = UUID.randomUUID().toString();

        EvotorShop evotorShop = new EvotorShop().setId(domainId).setEvotorUUID(shopUUID);
        Mockito.when(evotorShopService.findByDSystemDomainId(domainId)).thenReturn(evotorShop);

        EvotorUser evotorUser = new EvotorUser().setId(userId).setEvotorUserUUID(userUUID);
        Mockito.when(evotorUserService.findByDSystemUserId(userId)).thenReturn(evotorUser);

        EvotorPosUser evotorPosUser = new EvotorPosUser().setEvotorDeviceId(deviceUUID).setEvotorShop(evotorShop).setEvotorUser(evotorUser);
        Mockito.when(evotorPosUserService.findByEvotorUserAndEvotorShop(evotorShop, evotorUser)).thenReturn(evotorPosUser);

        Mockito.when(evotorServiceService.findByDSystemALTCode(any(String.class))).thenReturn(new EvotorService().setEvotorServiceUUID(UUID.randomUUID().toString()));

        String json = readPaymentJsonFromResources(domainId, userId);
        EvotorCheck evotorCheck = checkMappingService.map(new ClinicIQCheck().setCheckJson(json));
        assertNotNull("Mapped Receipt cannot be null", evotorCheck);
        assertEquals("Mapped Receipt shop UUID should match", shopUUID, evotorCheck.getShopUUID());
        assertEquals("Mapped Receipt user UUID should match", userUUID, evotorCheck.getUserUUID());
        assertEquals("Mapped Receipt device UUID should match", deviceUUID, evotorCheck.getDeviceUUID());
        assertNotNull("Mapped Receipt json cannot be null", evotorCheck.getCheckJson());
    }

    @Test
    public void testMapReceiptWithShop() throws IOException {
        Integer domainId = 12345;
        Integer userId = 98765;

        String shopUUID = UUID.randomUUID().toString();
        String deviceUUID = UUID.randomUUID().toString();

        EvotorShop evotorShop = new EvotorShop().setId(domainId).setEvotorUUID(shopUUID);
        Mockito.when(evotorShopService.findByDSystemDomainId(domainId)).thenReturn(evotorShop);

        Mockito.when(evotorUserService.findByDSystemUserId(userId)).thenReturn(null);

        EvotorPosUser evotorPosUser = new EvotorPosUser().setEvotorDeviceId(deviceUUID).setEvotorShop(evotorShop).setEvotorUser(null);
        Mockito.when(evotorPosUserService.findByEvotorUserAndEvotorShop(evotorShop, null)).thenReturn(null);
        Mockito.when(evotorPosUserService.findByEvotorShop(evotorShop)).thenReturn(newArrayList(evotorPosUser));

        Mockito.when(evotorServiceService.findByDSystemALTCode(any(String.class))).thenReturn(new EvotorService().setEvotorServiceUUID(UUID.randomUUID().toString()));

        String json = readPaymentJsonFromResources(domainId, userId);
        EvotorCheck evotorCheck = checkMappingService.map(new ClinicIQCheck().setCheckJson(json));
        assertNotNull("Mapped Receipt cannot be null", evotorCheck);
        assertEquals("Mapped Receipt shop UUID should match", shopUUID, evotorCheck.getShopUUID());
        assertEquals("Mapped Receipt device UUID should match", deviceUUID, evotorCheck.getDeviceUUID());
        assertNotNull("Mapped Receipt json cannot be null", evotorCheck.getCheckJson());
    }

    @Test(expected = CannotFindEvotorPosUserMappingException.class)
    public void testMapReceiptWithNothing() throws IOException {
        Integer domainId = 12345;
        Integer userId = 98765;

        Mockito.when(evotorShopService.findByDSystemDomainId(domainId)).thenReturn(null);
        Mockito.when(evotorUserService.findByDSystemUserId(userId)).thenReturn(null);

        Mockito.when(evotorServiceService.findByDSystemALTCode(any(String.class))).thenReturn(new EvotorService().setEvotorServiceUUID(UUID.randomUUID().toString()));

        String json = readPaymentJsonFromResources(domainId, userId);
        checkMappingService.map(new ClinicIQCheck().setCheckJson(json));
    }
}
