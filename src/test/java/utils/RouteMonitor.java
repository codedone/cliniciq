package utils;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.model.ModelCamelContext;

public class RouteMonitor {

    private final MockEndpoint resultEndpoint;

    public RouteMonitor(final String routeId, CamelContext camelContext) throws Exception {
        camelContext.getRouteDefinition(routeId).adviceWith((ModelCamelContext) camelContext,
                new AdviceWithRouteBuilder() {
                    @Override
                    public void configure(){
                        weaveAddFirst().wireTap("mock:end_" + routeId);
                    }
                });
        resultEndpoint = camelContext.getEndpoint("mock:end_" + routeId, MockEndpoint.class);
    }

    public MockEndpoint getResultEndpoint() {
        return resultEndpoint;
    }
}