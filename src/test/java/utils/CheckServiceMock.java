package utils;

import cliniciq.evotor.db.service.cashbox.ClinicIQCheckService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
public class CheckServiceMock {
    @Bean
    @Primary
    public ClinicIQCheckService clinicIQCheckService() {
        return Mockito.mock(ClinicIQCheckService.class);
    }
}
