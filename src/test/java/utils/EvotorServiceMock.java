package utils;

import cliniciq.evotor.db.service.cashbox.EvotorPosUserService;
import cliniciq.evotor.db.service.cashbox.EvotorServiceService;
import cliniciq.evotor.db.service.cashbox.EvotorShopService;
import cliniciq.evotor.db.service.cashbox.EvotorUserService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
public class EvotorServiceMock {
    @Bean
    @Primary
    public EvotorPosUserService evotorPosUserService() {
        return Mockito.mock(EvotorPosUserService.class);
    }

    @Bean
    @Primary
    public EvotorServiceService evotorServiceService() {
        return Mockito.mock(EvotorServiceService.class);
    }

    @Bean
    @Primary
    public EvotorShopService evotorShopService() {
        return Mockito.mock(EvotorShopService.class);
    }

    @Bean
    @Primary
    public EvotorUserService evotorUserService() {
        return Mockito.mock(EvotorUserService.class);
    }
}
