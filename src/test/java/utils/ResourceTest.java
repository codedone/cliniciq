package utils;

import org.json.JSONObject;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public abstract class ResourceTest {

    protected String readPaymentJsonFromResources(Integer domainId, Integer userId) throws IOException {
        File file = ResourceUtils.getFile(this.getClass().getResource("/requests/payment.json"));
        String json = new String(Files.readAllBytes(file.toPath()));

        JSONObject jsonObject = new JSONObject(json);
        JSONObject jsonPageWrapper = jsonObject.getJSONObject("pageWrapper");
        JSONObject dataJsonObject = jsonPageWrapper.getJSONObject("data");

        dataJsonObject = dataJsonObject.put("extra", new JSONObject().put("domainId", domainId).put("userId", userId));
        jsonPageWrapper = jsonPageWrapper.put("data", dataJsonObject);
        jsonObject.put("pageWrapper", jsonPageWrapper);

        return jsonObject.toString();
    }
}
