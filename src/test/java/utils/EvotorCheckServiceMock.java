package utils;

import cliniciq.evotor.db.service.rest.EvotorCheckService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
public class EvotorCheckServiceMock {
    @Bean
    @Primary
    public EvotorCheckService evotorCheckService() {
        return Mockito.mock(EvotorCheckService.class);
    }
}
