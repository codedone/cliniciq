package integration;

import cliniciq.evotor.EntryPoint;
import cliniciq.evotor.db.service.cashbox.EvotorPosUserService;
import cliniciq.evotor.db.service.cashbox.EvotorServiceService;
import cliniciq.evotor.db.service.cashbox.EvotorShopService;
import cliniciq.evotor.db.service.cashbox.EvotorUserService;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.kafka.KafkaConstants;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import utils.RouteMonitor;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {EntryPoint.class},
        properties = {"service.port=9099",
                "service.kafka.url=localhost",
                "service.kafka.port=9095",
                "spring.main.allow-bean-definition-overriding=true",
                "spring.jpa.hibernate.ddl-auto=create-drop"},
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles(profiles = {"db-integration-test"})
@EmbeddedKafka(partitions = 1, topics = {
        "Evotor_Goods_Dsystem",
        "Evotor_POS_Dsystem",
        "Evotor_User_Dsystem",
        "Evotor_shop_Dsystem",
        "Evotor_Prepared_Check_Dsystem"},
        brokerProperties = {"listeners=PLAINTEXT://localhost:9095", "port=9095"},
        controlledShutdown = true)
@EnableCaching
@EnableJpaRepositories(basePackages = {
        "cliniciq.evotor.db.repository"
})
@EnableTransactionManagement
@DirtiesContext
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NotificationTest {

    private static final String TEST_SIDE_SERVICE_TOKEN = "TOKEN-TOKEN-TOKEN-TOKEN";

    @Autowired
    private CamelContext camelContext;

    @Autowired
    private ProducerTemplate producerTemplate;

    @Autowired
    private EvotorShopService evotorShopService;

    @Autowired
    private EvotorUserService evotorUserService;

    @Autowired
    private EvotorServiceService evotorServiceService;

    @Autowired
    private EvotorPosUserService evotorPosUserService;

    @BeforeClass
    public static void beforeTest() {
        System.setProperty("evotor.side.service.authentication.token", TEST_SIDE_SERVICE_TOKEN);
        System.setProperty(EmbeddedKafkaBroker.BROKER_LIST_PROPERTY, "spring.kafka.bootstrap-servers");
    }

    @Test
    @DirtiesContext
    public void evotorShopDsystemKafkaConsumerTest() throws Exception {
        String shopFistUUID = UUID.randomUUID().toString();
        JSONObject jsonObjectFirst = new JSONObject();
        jsonObjectFirst.put("uuid", shopFistUUID);

        String shopSecondUUID = UUID.randomUUID().toString();
        JSONObject jsonObjectSecond = new JSONObject();
        jsonObjectSecond.put("uuid", shopSecondUUID);

        JSONObject[] array = {jsonObjectFirst, jsonObjectSecond};
        JSONArray arrayOfShops = new JSONArray(array);

        String message = arrayOfShops.toString();
        RouteMonitor routeMonitor = new RouteMonitor("evotor_store_dsystem_kafka_consumer_processor", camelContext);
        producerTemplate.sendBodyAndHeader("kafka:Evotor_Store_Dsystem", message, KafkaConstants.KEY, UUID.randomUUID().toString());

        routeMonitor.getResultEndpoint().expectedBodiesReceived(message);
        routeMonitor.getResultEndpoint().assertIsSatisfied(3000);

        TimeUnit.SECONDS.sleep(1);

        assertEquals("Shop should be created in db", shopFistUUID, evotorShopService.findByEvotorShopUUID(shopFistUUID).getEvotorUUID());
        assertEquals("Shop should be created in db", shopSecondUUID, evotorShopService.findByEvotorShopUUID(shopSecondUUID).getEvotorUUID());
    }

    @Test
    @DirtiesContext
    public void evotorUserDsystemKafkaConsumerTest() throws Exception {
        String shopUUID = UUID.randomUUID().toString();

        JSONObject shopJsonObject = new JSONObject();
        shopJsonObject.put("uuid", shopUUID);

        String shopMessage = shopJsonObject.toString();
        RouteMonitor shopRouteMonitor = new RouteMonitor("evotor_store_dsystem_kafka_consumer_processor", camelContext);
        producerTemplate.sendBodyAndHeader("kafka:Evotor_Store_Dsystem", shopMessage, KafkaConstants.KEY, UUID.randomUUID().toString());

        shopRouteMonitor.getResultEndpoint().expectedBodiesReceived(shopMessage);
        shopRouteMonitor.getResultEndpoint().assertIsSatisfied(3000);

        TimeUnit.SECONDS.sleep(1);

        JSONObject shop = new JSONObject();
        shop.put("storeUuid", shopUUID);

        String userUUID = UUID.randomUUID().toString();
        JSONObject jsonObjectUser = new JSONObject();
        jsonObjectUser.put("uuid", userUUID);
        JSONObject[] arrayOfShops = {shop};
        jsonObjectUser.put("stores", new JSONArray(arrayOfShops));

        JSONObject[] arrayOfUsers = {jsonObjectUser};
        JSONArray jsonArrayOfUsers = new JSONArray(arrayOfUsers);

        String userMessage = jsonArrayOfUsers.toString();
        RouteMonitor userRouteMonitor = new RouteMonitor("evotor_user_dsystem_kafka_consumer_processor", camelContext);
        producerTemplate.sendBodyAndHeader("kafka:Evotor_User_Dsystem", userMessage, KafkaConstants.KEY, UUID.randomUUID().toString());

        userRouteMonitor.getResultEndpoint().expectedBodiesReceived(userMessage);
        userRouteMonitor.getResultEndpoint().assertIsSatisfied(3000);

        TimeUnit.SECONDS.sleep(1);

        assertTrue("Shop should contain user", evotorShopService.findByEvotorShopUUID(shopUUID)
                .getEvotorUsers().stream()
                .anyMatch(evotorUser -> userUUID.equals(evotorUser.getEvotorUserUUID())));

        assertTrue("User should contain shop", evotorUserService.findByEvotorUserUUID(userUUID)
                .getEvotorShops().stream()
                .anyMatch(evotorShop -> shopUUID.equals(evotorShop.getEvotorUUID())));
    }

    @Test
    @DirtiesContext
    public void evotorGoodsDsystemKafkaConsumerTest() throws Exception {
        String shopUUID = UUID.randomUUID().toString();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("uuid", shopUUID);

        JSONObject[] array = {jsonObject};
        JSONArray arrayOfShops = new JSONArray(array);

        String shopMessage = arrayOfShops.toString();
        RouteMonitor shopRouteMonitor = new RouteMonitor("evotor_store_dsystem_kafka_consumer_processor", camelContext);
        producerTemplate.sendBodyAndHeader("kafka:Evotor_Store_Dsystem", shopMessage, KafkaConstants.KEY, UUID.randomUUID().toString());

        shopRouteMonitor.getResultEndpoint().expectedBodiesReceived(shopMessage);
        shopRouteMonitor.getResultEndpoint().assertIsSatisfied(3000);

        TimeUnit.SECONDS.sleep(1);

        String productUUID = UUID.randomUUID().toString();
        JSONObject jsonObjectProduct = new JSONObject();
        jsonObjectProduct.put("uuid", productUUID);
        jsonObjectProduct.put("type", "ALCOHOL_NOT_MARKED");
        jsonObjectProduct.put("measureName", "шт");
        JSONObject[] jsonArrayOfProducts = {jsonObjectProduct};
        JSONArray arrayOfProducts = new JSONArray(jsonArrayOfProducts);
        String productMessage = arrayOfProducts.toString();

        RouteMonitor productRouteMonitor = new RouteMonitor("evotor_goods_dsystem_kafka_consumer_processor", camelContext);
        producerTemplate.sendBodyAndHeader("kafka:Evotor_Goods_Dsystem", productMessage, "storeUUID", shopUUID);

        productRouteMonitor.getResultEndpoint().expectedBodiesReceived(productMessage);
        productRouteMonitor.getResultEndpoint().assertIsSatisfied(3000);

        TimeUnit.SECONDS.sleep(1);

        assertEquals("Product should contain shop", shopUUID, evotorServiceService.findByEvotorServiceUUID(productUUID).getEvotorShop().getEvotorUUID());
    }

    @Test
    @DirtiesContext
    public void evotorPosDsystemKafkaConsumerTest() throws Exception {
        String shopUUID = UUID.randomUUID().toString();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("uuid", shopUUID);

        JSONObject[] array = {jsonObject};
        JSONArray arrayOfShops = new JSONArray(array);

        String shopMessage = arrayOfShops.toString();
        RouteMonitor shopRouteMonitor = new RouteMonitor("evotor_store_dsystem_kafka_consumer_processor", camelContext);
        producerTemplate.sendBodyAndHeader("kafka:Evotor_Store_Dsystem", shopMessage, KafkaConstants.KEY, UUID.randomUUID().toString());

        shopRouteMonitor.getResultEndpoint().expectedBodiesReceived(shopMessage);
        shopRouteMonitor.getResultEndpoint().assertIsSatisfied(3000);

        TimeUnit.SECONDS.sleep(1);

        String posUUID = UUID.randomUUID().toString();
        JSONObject jsonObjectPos = new JSONObject();
        jsonObjectPos.put("uuid", posUUID);
        jsonObjectPos.put("storeUuid", shopUUID);
        JSONObject[] jsonArrayOfProducts = {jsonObjectPos};
        JSONArray arrayOfPos = new JSONArray(jsonArrayOfProducts);
        String posMessage = arrayOfPos.toString();

        RouteMonitor routeMonitor = new RouteMonitor("evotor_pos_dsystem_kafka_consumer_processor", camelContext);
        producerTemplate.sendBodyAndHeader("kafka:Evotor_POS_Dsystem", posMessage, KafkaConstants.KEY, UUID.randomUUID().toString());

        routeMonitor.getResultEndpoint().expectedBodiesReceived(posMessage);
        routeMonitor.getResultEndpoint().assertIsSatisfied(3000);

        TimeUnit.SECONDS.sleep(1);

        assertEquals("Product should contain shop", shopUUID, evotorPosUserService.findByEvotorDeviceId(posUUID).getEvotorShop().getEvotorUUID());
    }

    @Test
    @DirtiesContext
    public void evotorPreparedCheckDsystemKafkaConsumerTest() throws Exception {
        String shopUUID = UUID.randomUUID().toString();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("uuid", shopUUID);

        JSONObject[] array = {jsonObject};
        JSONArray arrayOfShops = new JSONArray(array);

        String message = arrayOfShops.toString();
        RouteMonitor routeMonitor = new RouteMonitor("evotor_prepared_check_dsystem_kafka_consumer_processor", camelContext);
        producerTemplate.sendBodyAndHeader("kafka:Evotor_Prepared_Check_Dsystem", message, KafkaConstants.KEY, UUID.randomUUID().toString());

        routeMonitor.getResultEndpoint().expectedBodiesReceived(message);
        routeMonitor.getResultEndpoint().assertIsSatisfied(3000);
    }
}
