package integration;

import cliniciq.evotor.EntryPoint;
import cliniciq.evotor.db.domain.cashbox.EvotorShop;
import cliniciq.evotor.db.domain.cashbox.EvotorUser;
import cliniciq.evotor.db.domain.rest.EvotorSideServiceUser;
import cliniciq.evotor.db.service.cashbox.EvotorShopService;
import cliniciq.evotor.db.service.cashbox.EvotorUserService;
import cliniciq.evotor.db.service.rest.EvotorSideServiceUserService;
import com.jayway.jsonpath.JsonPath;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.UUID;

import static cliniciq.evotor.utils.enums.HTTPHeaderDictionary.AUTHORIZATION;
import static cliniciq.evotor.utils.enums.HTTPHeaderDictionary.X_EVOTOR_STORE_UUID;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {EntryPoint.class},
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        properties = {"spring.jpa.hibernate.ddl-auto=create"})
@ActiveProfiles(profiles = {"db-integration-test", "mock", "no_kafka"})
@EnableCaching
@EnableJpaRepositories(basePackages = {
        "cliniciq.evotor.db.repository"
})
@EnableTransactionManagement
@DirtiesContext
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EvotorSideServiceUserRegistrationDBTest {

    private static final Integer PORT = 9099;
    private static final String TEST_SIDE_SERVICE_TOKEN = "TOKEN-TOKEN-TOKEN-TOKEN";
    @Autowired
    private EvotorSideServiceUserService evotorSideServiceUserService;

    @Autowired
    private EvotorShopService evotorShopService;

    @Autowired
    private EvotorUserService evotorUserService;

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeClass
    public static void beforeTest() {
        System.setProperty("evotor.side.service.authentication.token", TEST_SIDE_SERVICE_TOKEN);
        System.setProperty("service.port", PORT.toString());
    }

    @Test
    @DirtiesContext
    public void webSideServiceUserCreationTest() {
        String shopUUID = UUID.randomUUID().toString();
        String userId = UUID.randomUUID().toString();

        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTHORIZATION, TEST_SIDE_SERVICE_TOKEN);
        headers.add(X_EVOTOR_STORE_UUID, shopUUID);

        String json = "{\"userId\":\"" + userId + "\"}";
        HttpEntity<String> request = new HttpEntity<>(json, headers);

        HttpEntity<String> response = restTemplate.exchange("http://localhost:" + PORT + "/api/v1/user/created", HttpMethod.POST, request, String.class);
        String receivedToken = JsonPath.read(response.getBody(), "$.token");

        EvotorShop shopInDB = evotorShopService.findByEvotorShopUUID(shopUUID);
        assertNotNull("Loaded Shop should be saved in DB", shopInDB);

        EvotorUser userInDB = evotorUserService.findByEvotorUserUUID(userId);
        assertNotNull("Loaded User should be saved in DB", userInDB);

        EvotorSideServiceUser userStoredOInDB = evotorSideServiceUserService.findByUserId(userId);
        assertEquals("Loaded Token should be the same as in DB", receivedToken, userStoredOInDB.getToken());
        assertEquals("Loaded UserId should be the same as in DB", userId, userStoredOInDB.getUserId());
        assertEquals("Loaded ShopId should be the same as in DB", shopUUID, userStoredOInDB.getShopId());
    }

    @Test
    @DirtiesContext
    public void webSideServiceUserCreationWithExistingShopTest() {
        String shopUUID = UUID.randomUUID().toString();
        String userId = UUID.randomUUID().toString();

        evotorShopService.save(new EvotorShop().setEvotorUUID(shopUUID)
                .setEvotorUsers(newHashSet(new EvotorUser().setEvotorUserUUID(UUID.randomUUID().toString()))));

        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTHORIZATION, TEST_SIDE_SERVICE_TOKEN);
        headers.add(X_EVOTOR_STORE_UUID, shopUUID);

        String json = "{\"userId\":\"" + userId + "\"}";
        HttpEntity<String> request = new HttpEntity<>(json, headers);

        HttpEntity<String> response = restTemplate.exchange("http://localhost:" + PORT + "/api/v1/user/created", HttpMethod.POST, request, String.class);
        String receivedToken = JsonPath.read(response.getBody(), "$.token");

        EvotorShop shopInDB = evotorShopService.findByEvotorShopUUID(shopUUID);
        assertNotNull("Loaded Shop should be saved in DB", shopInDB);

        EvotorUser userInDB = evotorUserService.findByEvotorUserUUID(userId);
        assertNotNull("Loaded User should be saved in DB", userInDB);

        EvotorSideServiceUser userStoredOInDB = evotorSideServiceUserService.findByUserId(userId);
        assertEquals("Loaded Token should be the same as in DB", receivedToken, userStoredOInDB.getToken());
        assertEquals("Loaded UserId should be the same as in DB", userId, userStoredOInDB.getUserId());
        assertEquals("Loaded ShopId should be the same as in DB", shopUUID, userStoredOInDB.getShopId());
    }

    @Test
    @DirtiesContext
    public void webSideServiceUserCreationWithExistingShopAndUserTest() {
        String shopUUID = UUID.randomUUID().toString();
        String userId = UUID.randomUUID().toString();

        evotorShopService.save(new EvotorShop().setEvotorUUID(shopUUID)
                .setEvotorUsers(newHashSet(new EvotorUser().setEvotorUserUUID(userId))));

        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTHORIZATION, TEST_SIDE_SERVICE_TOKEN);
        headers.add(X_EVOTOR_STORE_UUID, shopUUID);

        String json = "{\"userId\":\"" + userId + "\"}";
        HttpEntity<String> request = new HttpEntity<>(json, headers);

        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + PORT + "/api/v1/user/created", HttpMethod.POST, request, String.class);
        int responseCode = response.getStatusCode().value();
        int code = JsonPath.read(response.getBody(), "$.code");
        assertEquals("Response code ", responseCode, 409);
        assertEquals("Error code should be", code, 2004);
    }
}
