package integration;

import cliniciq.evotor.EntryPoint;
import cliniciq.evotor.db.domain.cashbox.EvotorShop;
import cliniciq.evotor.db.domain.cashbox.EvotorUser;
import cliniciq.evotor.db.domain.rest.EvotorSideServiceUser;
import cliniciq.evotor.db.service.cashbox.EvotorShopService;
import cliniciq.evotor.db.service.rest.EvotorSideServiceUserService;
import com.jayway.jsonpath.JsonPath;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.UUID;

import static cliniciq.evotor.utils.enums.HTTPHeaderDictionary.AUTHORIZATION;
import static cliniciq.evotor.utils.enums.HTTPHeaderDictionary.X_EVOTOR_STORE_UUID;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {EntryPoint.class},
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        properties = {"spring.jpa.hibernate.ddl-auto=create"})
@ActiveProfiles(profiles = {"db-integration-test", "mock", "no_kafka"})
@EnableCaching
@EnableJpaRepositories(basePackages = {
        "cliniciq.evotor.db.repository"
})
@EnableTransactionManagement
@DirtiesContext
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EvotorSideServiceUserTokenDBTest {

    private static final Integer PORT = 9099;
    private static final String TEST_SIDE_SERVICE_TOKEN = "TOKEN-TOKEN-TOKEN-TOKEN";
    @Autowired
    private EvotorSideServiceUserService evotorSideServiceUserService;

    @Autowired
    private EvotorShopService evotorShopService;

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeClass
    public static void beforeTest() {
        System.setProperty("evotor.side.service.authentication.token", TEST_SIDE_SERVICE_TOKEN);
        System.setProperty("service.port", PORT.toString());
    }

    @Test
    @DirtiesContext
    public void webSideServiceUserTokenTest() {

        String shopUUID = UUID.randomUUID().toString();
        String userId = UUID.randomUUID().toString();

        evotorShopService.save(new EvotorShop().setEvotorUUID(shopUUID)
                .setEvotorUsers(newHashSet(new EvotorUser().setEvotorUserUUID(userId))));

        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTHORIZATION, TEST_SIDE_SERVICE_TOKEN);
        headers.add(X_EVOTOR_STORE_UUID, shopUUID);

        String name = "good";
        String password = "good";
        String json = "{\"userId\":\"" + userId + "\", \"username\":\"" + name + "\", \"password\":\"" + password + "\"}";
        HttpEntity<String> firstRequest = new HttpEntity<>(json, headers);

        HttpEntity<String> response = restTemplate.exchange("http://localhost:" + PORT + "/api/v1/user/verify", HttpMethod.POST, firstRequest, String.class);
        String token = JsonPath.read(response.getBody(), "$.token");
        String receivedUserId = JsonPath.read(response.getBody(), "$.userId");
        boolean hasBilling = JsonPath.read(response.getBody(), "$.hasBilling");

        assertNotNull("Loaded Token cannot be null", token);
        assertEquals("User ID should be the same", receivedUserId, userId);
        assertTrue("Has billing should be true", hasBilling);

        EvotorSideServiceUser userStoredOInDB = evotorSideServiceUserService.findByUserId(userId);
        assertNotNull("Loaded Token cannot be null", userStoredOInDB.getToken());
        assertEquals("Received token should match with db token", userStoredOInDB.getToken(), token);
        assertTrue("Has billing should be true", userStoredOInDB.isHasBilling());
    }

    @Test
    @DirtiesContext
    public void webSideServiceUserAssociationIncorrectShopIdTest() {
        String shopUUID = UUID.randomUUID().toString();
        String userId = UUID.randomUUID().toString();

        evotorShopService.save(new EvotorShop().setEvotorUUID(UUID.randomUUID().toString())
                .setEvotorUsers(newHashSet(new EvotorUser().setEvotorUserUUID(userId))));

        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTHORIZATION, TEST_SIDE_SERVICE_TOKEN);
        headers.add(X_EVOTOR_STORE_UUID, shopUUID);

        String name = "good";
        String password = "good";
        String json = "{\"userId\":\"" + userId + "\", \"username\":\"" + name + "\", \"password\":\"" + password + "\"}";
        HttpEntity<String> request = new HttpEntity<>(json, headers);

        ResponseEntity<String> exceptionResponse = restTemplate.exchange("http://localhost:" + PORT + "/api/v1/user/verify", HttpMethod.POST, request, String.class);
        int responseCode = exceptionResponse.getStatusCode().value();
        int code = JsonPath.read(exceptionResponse.getBody(), "$.code");
        assertEquals("Response code ", responseCode, 409);
        assertEquals("Error code should be", code, 2004);
    }

    @Test
    @DirtiesContext
    public void webSideServiceUserAssociationIncorrectUserIdTest() {
        String shopUUID = UUID.randomUUID().toString();
        String userId = UUID.randomUUID().toString();

        evotorShopService.save(new EvotorShop().setEvotorUUID(shopUUID)
                .setEvotorUsers(newHashSet(new EvotorUser().setEvotorUserUUID(UUID.randomUUID().toString()))));

        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTHORIZATION, TEST_SIDE_SERVICE_TOKEN);
        headers.add(X_EVOTOR_STORE_UUID, shopUUID);

        String name = "good";
        String password = "good";
        String json = "{\"userId\":\"" + userId + "\", \"username\":\"" + name + "\", \"password\":\"" + password + "\"}";
        HttpEntity<String> request = new HttpEntity<>(json, headers);

        ResponseEntity<String> exceptionResponse = restTemplate.exchange("http://localhost:" + PORT + "/api/v1/user/verify", HttpMethod.POST, request, String.class);
        int responseCode = exceptionResponse.getStatusCode().value();
        int code = JsonPath.read(exceptionResponse.getBody(), "$.code");
        assertEquals("Response code ", responseCode, 409);
        assertEquals("Error code should be", code, 2004);
    }

    @Test
    @DirtiesContext
    public void webSideServiceUserSecondAuthorizationTest() {
        String shopUUID = UUID.randomUUID().toString();
        String userIdFirst = UUID.randomUUID().toString();

        evotorShopService.save(new EvotorShop().setEvotorUUID(shopUUID)
                .setEvotorUsers(newHashSet(new EvotorUser().setEvotorUserUUID(userIdFirst))));

        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTHORIZATION, TEST_SIDE_SERVICE_TOKEN);
        headers.add(X_EVOTOR_STORE_UUID, shopUUID);

        String nameFirst = "good";
        String passwordFirst = "good";
        String jsonFirst = "{\"userId\":\"" + userIdFirst + "\", \"username\":\"" + nameFirst + "\", \"password\":\"" + passwordFirst + "\"}";
        HttpEntity<String> requestFirst = new HttpEntity<>(jsonFirst, headers);

        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + PORT + "/api/v1/user/verify", HttpMethod.POST, requestFirst, String.class);
        String token = JsonPath.read(response.getBody(), "$.token");
        String receivedUserId = JsonPath.read(response.getBody(), "$.userId");
        boolean hasBilling = JsonPath.read(response.getBody(), "$.hasBilling");

        assertNotNull("Loaded Token cannot be null", token);
        assertEquals("User ID should be the same", receivedUserId, userIdFirst);
        assertTrue("Has billing should be true", hasBilling);

        String nameSecond = "good";
        String passwordSecond = "good";
        String jsonSecond = "{\"userId\":\"" + userIdFirst + "\", \"username\":\"" + nameSecond + "\", \"password\":\"" + passwordSecond + "\"}";
        HttpEntity<String> requestSecond = new HttpEntity<>(jsonSecond, headers);

        ResponseEntity<String> exceptionResponse = restTemplate.exchange("http://localhost:" + PORT + "/api/v1/user/verify", HttpMethod.POST, requestSecond, String.class);
        String tokenSecond = JsonPath.read(response.getBody(), "$.token");
        assertEquals("Valid token should match", token, tokenSecond);
    }

    @Test
    @DirtiesContext
    public void webSideServiceUserSecondAuthorizationWithDifferentUserTest() {
        String shopUUID = UUID.randomUUID().toString();
        String userIdFirst = UUID.randomUUID().toString();
        String userIdSecond = UUID.randomUUID().toString();

        evotorShopService.save(new EvotorShop().setEvotorUUID(shopUUID)
                .setEvotorUsers(newHashSet(new EvotorUser().setEvotorUserUUID(userIdFirst))));

        HttpHeaders headersFirst = new HttpHeaders();
        headersFirst.add(AUTHORIZATION, TEST_SIDE_SERVICE_TOKEN);
        headersFirst.add(X_EVOTOR_STORE_UUID, shopUUID);

        String nameFirst = "good";
        String passwordFirst = "good";
        String jsonFirst = "{\"userId\":\"" + userIdFirst + "\", \"username\":\"" + nameFirst + "\", \"password\":\"" + passwordFirst + "\"}";
        HttpEntity<String> requestFirst = new HttpEntity<>(jsonFirst, headersFirst);

        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + PORT + "/api/v1/user/verify", HttpMethod.POST, requestFirst, String.class);
        String token = JsonPath.read(response.getBody(), "$.token");
        String receivedUserId = JsonPath.read(response.getBody(), "$.userId");
        boolean hasBilling = JsonPath.read(response.getBody(), "$.hasBilling");

        assertNotNull("Loaded Token cannot be null", token);
        assertEquals("User ID should be the same", receivedUserId, userIdFirst);
        assertTrue("Has billing should be true", hasBilling);

        HttpHeaders headerSecond = new HttpHeaders();
        headerSecond.add(AUTHORIZATION, TEST_SIDE_SERVICE_TOKEN);
        headerSecond.add(X_EVOTOR_STORE_UUID, shopUUID);

        String nameSecond = "mood";
        String passwordSecond = "mood";
        String jsonSecond = "{\"userId\":\"" + userIdSecond + "\", \"username\":\"" + nameSecond + "\", \"password\":\"" + passwordSecond + "\"}";
        HttpEntity<String> requestSecond = new HttpEntity<>(jsonSecond, headerSecond);

        ResponseEntity<String> exceptionResponse = restTemplate.exchange("http://localhost:" + PORT + "/api/v1/user/verify", HttpMethod.POST, requestSecond, String.class);
        int responseCode = exceptionResponse.getStatusCode().value();
        assertEquals("Response code ", responseCode, 409);
    }

    @Test
    @DirtiesContext
    public void webTokenUnauthorizedException() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTHORIZATION, "");
        headers.add(X_EVOTOR_STORE_UUID, UUID.randomUUID().toString());

        String userId = "231543513";
        String json = "{\"userId\":\"" + userId + "\", \"username\":\"unauthorized\", \"password\":\"unauthorized\"}";
        HttpEntity<String> firstRequest = new HttpEntity<>(json, headers);

        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + PORT + "/api/v1/user/verify", HttpMethod.POST, firstRequest, String.class);
        int responseCode = response.getStatusCode().value();
        int code = JsonPath.read(response.getBody(), "$.code");
        assertEquals("Response code ", responseCode, 401);
        assertEquals("Error code should be", code, 1001);
        assertNull(evotorSideServiceUserService.findByUserId(userId));
    }

    @Test
    @DirtiesContext
    public void webUserUnauthorizedException() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTHORIZATION, TEST_SIDE_SERVICE_TOKEN);

        String userId = "3514614534";
        String json = "{\"userId\":\"" + userId + "\", \"username\":\"unauthorized\", \"password\":\"unauthorized\"}";
        HttpEntity<String> firstRequest = new HttpEntity<>(json, headers);

        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + PORT + "/api/v1/user/verify", HttpMethod.POST, firstRequest, String.class);
        int responseCode = response.getStatusCode().value();
        int code = JsonPath.read(response.getBody(), "$.code");
        assertEquals("Response code ", responseCode, 401);
        assertEquals("Error code should be", code, 1006);
        assertNull(evotorSideServiceUserService.findByUserId(userId));
    }

    @Test
    @DirtiesContext
    public void webUserIncorrectFieldsException() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTHORIZATION, TEST_SIDE_SERVICE_TOKEN);
        headers.add(X_EVOTOR_STORE_UUID, UUID.randomUUID().toString());

        String userId = "567265243";
        String json = "{\"userIdd\":\"" + userId + "\", \"username\":\"unauthorized\", \"password\":\"unauthorized\"}";
        HttpEntity<String> firstRequest = new HttpEntity<>(json, headers);

        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + PORT + "/api/v1/user/verify", HttpMethod.POST, firstRequest, String.class);
        int responseCode = response.getStatusCode().value();
        int code = JsonPath.read(response.getBody(), "$.code");
        assertEquals("Response code ", responseCode, 400);
        assertEquals("Error code should be", code, 2002);
        assertNull(evotorSideServiceUserService.findByUserId(userId));
    }
}
