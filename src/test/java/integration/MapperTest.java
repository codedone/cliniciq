package integration;

import cliniciq.evotor.EntryPoint;
import cliniciq.evotor.config.KafkaRouteConfiguration;
import cliniciq.evotor.db.domain.cashbox.EvotorPosUser;
import cliniciq.evotor.db.domain.cashbox.EvotorService;
import cliniciq.evotor.db.domain.cashbox.EvotorShop;
import cliniciq.evotor.db.domain.cashbox.EvotorUser;
import cliniciq.evotor.db.domain.enums.CheckStatus;
import cliniciq.evotor.db.domain.rest.EvotorCheck;
import cliniciq.evotor.db.service.cashbox.EvotorPosUserService;
import cliniciq.evotor.db.service.cashbox.EvotorServiceService;
import cliniciq.evotor.db.service.cashbox.EvotorShopService;
import cliniciq.evotor.db.service.cashbox.EvotorUserService;
import cliniciq.evotor.db.service.rest.EvotorCheckService;
import com.jayway.jsonpath.JsonPath;
import org.apache.camel.ProducerTemplate;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import utils.ResourceTest;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static cliniciq.evotor.utils.enums.HTTPHeaderDictionary.*;
import static cliniciq.evotor.utils.enums.KafkaRouteType.PRODUCER;
import static com.google.common.collect.Sets.newHashSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = {EntryPoint.class},
        properties = {"service.port=9099",
                "service.kafka.url=localhost",
                "service.kafka.port=9095",
                "spring.main.allow-bean-definition-overriding=true",
                "spring.jpa.hibernate.ddl-auto=create-drop"},
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles(profiles = {"db-integration-test"})
@EmbeddedKafka(partitions = 1, topics = {"Evotor_Payment_Dsystem"},
        brokerProperties = {"listeners=PLAINTEXT://localhost:9095", "port=9095"},
        controlledShutdown = true)
@EnableCaching
@EnableJpaRepositories(basePackages = {
        "cliniciq.evotor.db.repository"
})
@EnableTransactionManagement
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MapperTest extends ResourceTest {

    private static final Integer PORT = 9099;
    private static final String TEST_SIDE_SERVICE_TOKEN = "TOKEN-TOKEN-TOKEN-TOKEN";

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private EvotorCheckService evotorCheckService;

    @Autowired
    private ProducerTemplate producerTemplate;

    @Autowired
    private EvotorShopService evotorShopService;

    @Autowired
    private EvotorUserService evotorUserService;

    @Autowired
    private EvotorPosUserService evotorPosUserService;

    @Autowired
    private EvotorServiceService evotorServiceService;

    @Autowired
    private KafkaRouteConfiguration kafkaRouteConfiguration;

    @BeforeClass
    public static void beforeTest() {
        System.setProperty("evotor.side.service.authentication.token", TEST_SIDE_SERVICE_TOKEN);
        System.setProperty("service.port", PORT.toString());
    }

    @Test
    @DirtiesContext
    public void testFindCheck() {
        String shopId = UUID.randomUUID().toString();
        String userId = UUID.randomUUID().toString();
        String deviceId = UUID.randomUUID().toString();

        String json = "{\"key\": \"key\"}";
        evotorCheckService.save(new EvotorCheck()
                .setShopUUID(shopId)
                .setUserUUID(userId)
                .setDeviceUUID(deviceId)
                .setCheckStatus(CheckStatus.PROCESSED).setCheckJson(json));

        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTHORIZATION, TEST_SIDE_SERVICE_TOKEN);
        headers.add(X_EVOTOR_STORE_UUID, shopId);
        headers.add(X_EVOTOR_USER_ID, userId);
        headers.add(X_EVOTOR_DEVICE_UUID, deviceId);

        HttpEntity<String> firstRequest = new HttpEntity<>("", headers);
        HttpEntity<String> response = restTemplate.exchange("http://localhost:" + PORT + "/api/payment/check", HttpMethod.POST, firstRequest, String.class);

        assertEquals("Check in Received Message should be the same as in DB", response.getBody(), json);
        List<EvotorCheck> evotorChecks = evotorCheckService.findByShopUUIDAndUserUUID(shopId, userId);
        assertEquals("Check Status should be SEND", evotorChecks.stream()
                .findFirst()
                .map(EvotorCheck::getCheckStatus)
                .orElse(null), CheckStatus.SEND);
    }

    @Test
    @DirtiesContext
    public void testFullMapperCycle() throws IOException, InterruptedException {
        String domainId = UUID.randomUUID().toString();
        Integer clinicIQDomainId = new Random().nextInt();
        String userId = UUID.randomUUID().toString();
        Integer clinicIQUserId = new Random().nextInt();
        String deviceId = UUID.randomUUID().toString();

        EvotorUser evotorUser = evotorUserService.save(new EvotorUser().setEvotorUserUUID(userId).setDSystemUserId(clinicIQUserId));
        EvotorShop evotorShop = evotorShopService.save(new EvotorShop().setEvotorUUID(domainId).setDSystemDomainId(clinicIQDomainId));
        evotorShop = evotorShopService.save(evotorShop.setEvotorUsers(newHashSet(evotorUser)));

        evotorPosUserService.save(new EvotorPosUser().setEvotorDeviceId(deviceId).setEvotorShop(evotorShop).setEvotorUser(evotorUser));

        evotorServiceService.save(new EvotorService()
                .setEvotorServiceUUID(UUID.randomUUID().toString())
                .setType("")
                .setMeasureName("")
                .setDSystemALTCode("93ebb7b0-ce83-44c2-9893-218ab7ba8017")
                .setEvotorShop(evotorShop));

        evotorServiceService.save(new EvotorService()
                .setEvotorServiceUUID(UUID.randomUUID().toString())
                .setType("")
                .setMeasureName("")
                .setDSystemALTCode("5bac7f6d-3f85-431c-8a5d-0edfda148e28")
                .setEvotorShop(evotorShop));

        Map<String, Object> kafkaHeaders = new HashMap<>();
        kafkaHeaders.put("domain_id", clinicIQDomainId);
        kafkaHeaders.put("user_id", clinicIQUserId);

        String json = readPaymentJsonFromResources(clinicIQDomainId, clinicIQUserId);
        producerTemplate.sendBodyAndHeaders(kafkaRouteConfiguration.checkTopic(PRODUCER), json, kafkaHeaders);

        TimeUnit.SECONDS.sleep(5);

        List<EvotorCheck> evotorChecks = evotorCheckService.findByShopUUIDAndUserUUID(domainId, userId);
        assertEquals("Check Status should be Processed", evotorChecks.stream()
                .findFirst()
                .map(EvotorCheck::getCheckStatus)
                .orElse(null), CheckStatus.PROCESSED);

        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTHORIZATION, TEST_SIDE_SERVICE_TOKEN);
        headers.add(X_EVOTOR_STORE_UUID, domainId);
        headers.add(X_EVOTOR_USER_ID, userId);
        headers.add(X_EVOTOR_DEVICE_UUID, deviceId);

        HttpEntity<String> firstRequest = new HttpEntity<>("", headers);
        HttpEntity<String> response = restTemplate.exchange("http://localhost:" + PORT + "/api/payment/check", HttpMethod.POST, firstRequest, String.class);

        assertNotNull("Check in Received Message should be present", response.getBody());
        evotorChecks = evotorCheckService.findByShopUUIDAndUserUUID(domainId, userId);
        assertEquals("Check Status should be SEND", evotorChecks.stream()
                .findFirst()
                .map(EvotorCheck::getCheckStatus)
                .orElse(null), CheckStatus.SEND);
    }

    @Test
    @DirtiesContext
    public void testFullMapperCycleWithoutUser() throws IOException, InterruptedException {
        String domainId = UUID.randomUUID().toString();
        Integer clinicIQDomainId = new Random().nextInt();
        String deviceId = UUID.randomUUID().toString();

        EvotorUser evotorUser = evotorUserService.save(new EvotorUser().setEvotorUserUUID(UUID.randomUUID().toString()).setDSystemUserId(new Random().nextInt()));
        EvotorShop evotorShop = evotorShopService.save(new EvotorShop().setEvotorUUID(domainId).setDSystemDomainId(clinicIQDomainId));
        evotorShop = evotorShopService.save(evotorShop.setEvotorUsers(newHashSet(evotorUser)));

        evotorPosUserService.save(new EvotorPosUser().setEvotorDeviceId(deviceId).setEvotorShop(evotorShop).setEvotorUser(evotorUser));

        evotorServiceService.save(new EvotorService()
                .setEvotorServiceUUID(UUID.randomUUID().toString())
                .setType("")
                .setMeasureName("")
                .setDSystemALTCode("93ebb7b0-ce83-44c2-9893-218ab7ba8017")
                .setEvotorShop(evotorShop));

        evotorServiceService.save(new EvotorService()
                .setEvotorServiceUUID(UUID.randomUUID().toString())
                .setType("")
                .setMeasureName("")
                .setDSystemALTCode("5bac7f6d-3f85-431c-8a5d-0edfda148e28")
                .setEvotorShop(evotorShop));

        Map<String, Object> kafkaHeaders = new HashMap<>();
        String json = readPaymentJsonFromResources(clinicIQDomainId, null);
        producerTemplate.sendBodyAndHeaders(kafkaRouteConfiguration.checkTopic(PRODUCER), json, kafkaHeaders);

        TimeUnit.SECONDS.sleep(5);

        List<EvotorCheck> evotorChecks = evotorCheckService.findByShopUUIDAndUserUUID(domainId, null);
        assertEquals("Check Status should be Processed", evotorChecks.stream()
                .findFirst()
                .map(EvotorCheck::getCheckStatus)
                .orElse(null), CheckStatus.PROCESSED);

        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTHORIZATION, TEST_SIDE_SERVICE_TOKEN);
        headers.add(X_EVOTOR_STORE_UUID, domainId);
        headers.add(X_EVOTOR_DEVICE_UUID, deviceId);

        HttpEntity<String> firstRequest = new HttpEntity<>("", headers);
        HttpEntity<String> response = restTemplate.exchange("http://localhost:" + PORT + "/api/payment/check", HttpMethod.POST, firstRequest, String.class);

        assertNotNull("Check in Received Message should be present", response.getBody());
        evotorChecks = evotorCheckService.findByShopUUIDAndUserUUID(domainId, null);
        assertEquals("Check Status should be SEND", evotorChecks.stream()
                .findFirst()
                .map(EvotorCheck::getCheckStatus)
                .orElse(null), CheckStatus.SEND);
    }

    @Test
    @DirtiesContext
    public void testFullMapperCycleWithoutDomainAndUser() throws IOException, InterruptedException {
        String domainId = UUID.randomUUID().toString();
        Integer clinicIQDomainId = new Random().nextInt();
        String deviceId = UUID.randomUUID().toString();

        EvotorUser evotorUser = evotorUserService.save(new EvotorUser().setEvotorUserUUID(UUID.randomUUID().toString()).setDSystemUserId(new Random().nextInt()));
        EvotorShop evotorShop = evotorShopService.save(new EvotorShop().setEvotorUUID(domainId).setDSystemDomainId(clinicIQDomainId));
        evotorShop = evotorShopService.save(evotorShop.setEvotorUsers(newHashSet(evotorUser)));

        evotorPosUserService.save(new EvotorPosUser().setEvotorDeviceId(deviceId).setEvotorShop(evotorShop).setEvotorUser(evotorUser));

        evotorServiceService.save(new EvotorService()
                .setEvotorServiceUUID(UUID.randomUUID().toString())
                .setType("")
                .setMeasureName("")
                .setDSystemALTCode("93ebb7b0-ce83-44c2-9893-218ab7ba8017")
                .setEvotorShop(evotorShop));

        evotorServiceService.save(new EvotorService()
                .setEvotorServiceUUID(UUID.randomUUID().toString())
                .setType("")
                .setMeasureName("")
                .setDSystemALTCode("5bac7f6d-3f85-431c-8a5d-0edfda148e28")
                .setEvotorShop(evotorShop));

        Map<String, Object> kafkaHeaders = new HashMap<>();
        String json = readPaymentJsonFromResources(clinicIQDomainId, null);
        producerTemplate.sendBodyAndHeaders(kafkaRouteConfiguration.checkTopic(PRODUCER), json, kafkaHeaders);

        TimeUnit.SECONDS.sleep(5);

        List<EvotorCheck> evotorChecks = evotorCheckService.findByShopUUIDAndUserUUID(domainId, null);
        assertEquals("Check Status should be Processed", evotorChecks.stream()
                .findFirst()
                .map(EvotorCheck::getCheckStatus)
                .orElse(null), CheckStatus.PROCESSED);

        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTHORIZATION, TEST_SIDE_SERVICE_TOKEN);

        HttpEntity<String> firstRequest = new HttpEntity<>("", headers);
        ResponseEntity<String> response = restTemplate.exchange("http://localhost:" + PORT + "/api/payment/check", HttpMethod.POST, firstRequest, String.class);

        int responseCode = response.getStatusCode().value();
        assertEquals("Check in Received Message should be present", 500, responseCode);

        int resultCode = JsonPath.read(response.getBody(), "$.resultCode");
        assertEquals("Result code should match", 1, resultCode);

        String resultMessage = JsonPath.read(response.getBody(), "$.resultMessage");
        assertNotNull("Result message should be present", resultMessage);

        evotorChecks = evotorCheckService.findByShopUUIDAndUserUUID(domainId, null);
        assertEquals("Check Status should be SEND", evotorChecks.stream()
                .findFirst()
                .map(EvotorCheck::getCheckStatus)
                .orElse(null), CheckStatus.PROCESSED);
    }
}
