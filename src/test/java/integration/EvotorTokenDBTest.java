package integration;

import cliniciq.evotor.EntryPoint;
import cliniciq.evotor.db.domain.rest.EvotorToken;
import cliniciq.evotor.db.service.rest.EvotorTokenService;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {EntryPoint.class},
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        properties = {"spring.jpa.hibernate.ddl-auto=create"})
@ActiveProfiles(profiles = {"db-integration-test", "mock", "no_kafka"})
@DirtiesContext
@EnableJpaRepositories(basePackages = {
        "cliniciq.evotor.db.repository"
})
@EnableTransactionManagement
@EnableCaching
public class EvotorTokenDBTest {

    private static final Integer PORT = 9099;
    private static final String TEST_SIDE_SERVICE_TOKEN = "TOKEN-TOKEN-TOKEN-TOKEN";
    @Autowired
    private EvotorTokenService evotorTokenService;
    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeClass
    public static void beforeTest() {
        System.setProperty("evotor.side.service.authentication.token", TEST_SIDE_SERVICE_TOKEN);
        System.setProperty("service.port", PORT.toString());
    }

    @Test
    public void storeTokenTest() {
        String firstTokenString = UUID.randomUUID().toString();
        EvotorToken firstToken = evotorTokenService.save(new EvotorToken().setToken(firstTokenString));
        EvotorToken firstStoredToken = evotorTokenService.findLast();

        assertNotNull("Loaded Token cannot be null", firstStoredToken);
        assertEquals("Saved and Loaded token should match", firstToken.getToken(), firstStoredToken.getToken());

        String secondTokenString = UUID.randomUUID().toString();
        EvotorToken secondToken = evotorTokenService.save(new EvotorToken().setToken(secondTokenString));
        EvotorToken secondStoredToken = evotorTokenService.findLast();

        assertNotNull("Loaded Token cannot be null", secondStoredToken);
        assertNotEquals("Previous token cannot bey same as Loaded token", firstToken.getToken(), secondStoredToken.getToken());
        assertEquals("Saved and Loaded token should match", secondToken.getToken(), secondStoredToken.getToken());
    }

    @Test
    public void webStoreTokenTest() {
        String firstTokenString = UUID.randomUUID().toString();
        String firstEvotorTokenMessage = generateEvotorTokenMessage(firstTokenString);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", TEST_SIDE_SERVICE_TOKEN);
        HttpEntity<String> firstRequest = new HttpEntity<>(firstEvotorTokenMessage, headers);
        restTemplate.exchange("http://localhost:" + PORT + "/api/v1/user/token", HttpMethod.POST, firstRequest, String.class);

        EvotorToken firstStoredToken = evotorTokenService.findLast();
        assertNotNull("Loaded Token cannot be null", firstStoredToken);
        assertEquals("Saved and Loaded token should match", firstTokenString, firstStoredToken.getToken());

        String secondTokenString = UUID.randomUUID().toString();
        String secondEvotorTokenMessage = generateEvotorTokenMessage(secondTokenString);

        HttpEntity<String> secondRequest = new HttpEntity<>(secondEvotorTokenMessage, headers);
        restTemplate.exchange("http://localhost:" + PORT + "/api/v1/user/token", HttpMethod.POST, secondRequest, String.class);

        EvotorToken secondStoredToken = evotorTokenService.findLast();
        assertNotNull("Loaded Token cannot be null", secondStoredToken);
        assertNotEquals("Previous token cannot bey same as Loaded token", firstTokenString, secondStoredToken.getToken());
        assertEquals("Saved and Loaded token should match", secondTokenString, secondStoredToken.getToken());
    }

    @Test
    public void webStoreTokenTestException() {
        String firstTokenString = UUID.randomUUID().toString();
        String firstEvotorTokenMessage = generateEvotorTokenMessage(firstTokenString);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "");
        HttpEntity<String> firstRequest = new HttpEntity<>(firstEvotorTokenMessage, headers);
        ResponseEntity<String> exchange = restTemplate.exchange("http://localhost:" + PORT + "/api/v1/user/token", HttpMethod.POST, firstRequest, String.class);
        assertEquals(exchange.getStatusCode(), HttpStatus.UNAUTHORIZED);
    }

    private String generateEvotorTokenMessage(String token) {
        try {
            JSONObject tokenJson = new JSONObject();
            tokenJson.put("token", token);
            return tokenJson.toString();
        } catch (JSONException e) {
            throw new RuntimeException("Cannot create evotor token json message");
        }
    }
}
